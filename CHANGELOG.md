# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

### Changed

### Fixed

## [0.0.9] - 2025-01-22

### Added

### Changed

### Fixed

- Npm audit fix

## [0.0.8] - 2021-10-31

### Added

### Changed

### Fixed
-   Updating some dependencies

## [0.0.7] - 2021-02-07

### Added

### Changed

### Fixed

-   Fixing broken documentation generation (`Error: No entry points provided`)
-   Upgraded dependencies
-   Better code quality (less unused exports, less duplications, etc.)

## [0.0.6] - 2021-01-28

### Added

### Changed

### Fixed

-   Adding `CHANGELOG.md` file
-   Fixing example in the `README.md` file
-   Adding `LICENSE.md` file
-   The `lib` folder is now skipped by eslint
-   Dependency update
-   Better test coverage (100%)

## [0.0.5] - 2021-01-27

### Added

### Changed

### Fixed

-   Fixing null checking
-   Better test coverage

## [0.0.4] - 2021-01-26

### Added

### Changed

### Fixed

-   Fixing typo in `README.md`

## [0.0.3] - 2021-01-26

### Added

### Changed

### Fixed

-   Better project image in the `README.md` file

## [0.0.2] - 2021-01-26

### Added

### Changed

### Fixed

-   Marking the prototype available
-   Initial commit
