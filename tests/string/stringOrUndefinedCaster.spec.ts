import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    su: string | undefined;
}

const testCaster = createCaster<ITest>().addStringOrUndefined("su").build();

describe("String or undefined caster tests", () => {
    it("Check good string value", () => {
        const testData: unknown = {
            su: "test",
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good string value", () => {
        const testData: unknown = {
            su: "test",
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good undefined value", () => {
        const testData: unknown = {
            su: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good undefined value", () => {
        const testData: unknown = {
            su: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            su: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            su: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
