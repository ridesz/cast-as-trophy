import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("String array or undefined caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArrayOrUndefined("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Simple addStringArray is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArray("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("The addStringArrayOrUndefined for type without undefined is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArrayOrUndefined("s")' +
            "    .build();";
        testBadCode(code);
    });
});
