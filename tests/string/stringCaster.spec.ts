import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    s: string;
}

const testCaster = createCaster<ITest>().addString("s").build();

describe("String caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            s: "test",
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value", () => {
        const testData: unknown = {
            s: "test",
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            s: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            s: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value", () => {
        const testData: unknown = {
            s: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
