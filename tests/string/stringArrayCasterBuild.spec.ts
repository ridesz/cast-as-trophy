import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("String array caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArray("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArray("s")' +
            '    .addStringArray("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: number[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addStringArray("s")' +
            "    .build();";
        testBadCode(code);
    });
});
