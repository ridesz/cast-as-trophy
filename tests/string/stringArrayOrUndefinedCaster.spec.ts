import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    sau: string[] | undefined;
}

const testCaster = createCaster<ITest>().addStringArrayOrUndefined("sau").build();

describe("String array or undefined caster tests", () => {
    it("Check good value (not undefined)", () => {
        const testData: unknown = {
            sau: ["test", "test"],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (not undefined)", () => {
        const testData: unknown = {
            sau: ["test", "test"],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good value (undefined)", () => {
        const testData: unknown = {
            sau: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (undefined)", () => {
        const testData: unknown = {
            sau: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });

    it("Check bad value", () => {
        const testData: unknown = {
            sau: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            sau: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            sau: ["test", 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            sau: ["test", 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
