import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    sa: string[];
}

const testCaster = createCaster<ITest>().addStringArray("sa").build();

describe("String array caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            sa: ["test", "test"],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Check bad value (undefined)", () => {
        const testData: unknown = {
            sa: [undefined, "test"],
        };
        expect(isEqual(testData, testCaster.isType(testData))).is.equals(false);
    });
    it("Check bad value (null)", () => {
        const testData: unknown = {
            sa: [null, "test"],
        };
        expect(isEqual(testData, testCaster.isType(testData))).is.equals(false);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            sa: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            sa: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            sa: ["test", 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            sa: ["test", 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
