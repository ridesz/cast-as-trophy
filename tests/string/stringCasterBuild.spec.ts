import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("String caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addString("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: number;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type (with union)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("String literal is not enabled (single item, not marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            '    s: "TEST";' +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("String literal is not enabled (single item, marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            '    s: "OK";' +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("String literal is not enabled (union without marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            '    s: "TEST1" | "TEST2";' +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("String literal is not enabled (union with marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            '    s: "TEST" | "OK";' +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
});
