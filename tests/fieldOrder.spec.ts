import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../src";

interface ITestInner {
    s: string;
    b: boolean;
    n: number;
}

interface ITest {
    s: string;
    b: boolean;
    n: number;
    x: ITestInner;
    as: string[];
    an: number[];
    ab: boolean[];
}

const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

const testData: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: { s: "test", n: 1, b: false },
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

describe("Field order test", () => {
    it("Number, string, boolean... check", () => {
        const testCaster = createCaster<ITest>()
            .addNumber("n")
            .addString("s")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addNumberArray("an")
            .addStringArray("as")
            .addBooleanArray("ab")
            .build();
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Number, string, boolean... cast", () => {
        const testCaster = createCaster<ITest>()
            .addNumber("n")
            .addString("s")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addNumberArray("an")
            .addStringArray("as")
            .addBooleanArray("ab")
            .build();
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("String, number, boolean... check", () => {
        const testCaster = createCaster<ITest>()
            .addString("s")
            .addNumber("n")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addStringArray("as")
            .addNumberArray("an")
            .addBooleanArray("ab")
            .build();
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("String, number, boolean... cast", () => {
        const testCaster = createCaster<ITest>()
            .addString("s")
            .addNumber("n")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addStringArray("as")
            .addNumberArray("an")
            .addBooleanArray("ab")
            .build();
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
});
