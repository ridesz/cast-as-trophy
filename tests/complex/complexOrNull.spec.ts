import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
    b: boolean;
    n: number;
}

interface ITest {
    s: string;
    b: boolean;
    n: number;
    x: ITestInner | null;
    as: string[];
    an: number[];
    ab: boolean[];
}

const testDataWithoutNull: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: { s: "test", n: 1, b: false },
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithNull: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: null,
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithWrongInnerType: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: "BAD",
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

const testCaster = createCaster<ITest>()
    .addNumber("n")
    .addString("s")
    .addBoolean("b")
    .addCustomOrNull("x", testCasterInner)
    .addStringArray("as")
    .addBooleanArray("ab")
    .addNumberArray("an")
    .build();

describe("Complex or null object tests", () => {
    it("Complex or null object check (not null)", () => {
        expect(testCaster.isType(testDataWithoutNull)).is.equals(true);
    });
    it("Complex or null object cast (not null)", () => {
        expect(isEqual(testDataWithoutNull, testCaster.castTo(testDataWithoutNull))).is.equals(true);
    });
    it("Complex or null object check (null)", () => {
        expect(testCaster.isType(testDataWithNull)).is.equals(true);
    });
    it("Complex or null object cast (null)", () => {
        expect(isEqual(testDataWithNull, testCaster.castTo(testDataWithNull))).is.equals(true);
    });
    it("Complex or null object check (wrong inner type)", () => {
        expect(testCaster.isType(testDataWithWrongInnerType)).is.equals(false);
    });
    it("Complex or null object cast (wrong inner type)", () => {
        let result = true;
        try {
            isEqual(testDataWithWrongInnerType, testCaster.castTo(testDataWithWrongInnerType));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
