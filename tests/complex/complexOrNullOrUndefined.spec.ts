import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
    b: boolean;
    n: number;
}

interface ITest {
    s: string;
    b: boolean;
    n: number;
    x: ITestInner | null | undefined;
    as: string[];
    an: number[];
    ab: boolean[];
}

const testDataWithoutNullOrUndefined: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: { s: "test", n: 1, b: false },
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithNull: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: null,
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithUndefined: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: undefined,
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithWrongInnerType: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: "BAD",
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

const testCaster = createCaster<ITest>()
    .addNumber("n")
    .addString("s")
    .addBoolean("b")
    .addCustomOrNullOrUndefined("x", testCasterInner)
    .addStringArray("as")
    .addBooleanArray("ab")
    .addNumberArray("an")
    .build();

describe("Complex or null or null object tests", () => {
    it("Complex or null or null object check (not null or undefined)", () => {
        expect(testCaster.isType(testDataWithoutNullOrUndefined)).is.equals(true);
    });
    it("Complex or null or null object cast (not null or undefined)", () => {
        expect(isEqual(testDataWithoutNullOrUndefined, testCaster.castTo(testDataWithoutNullOrUndefined))).is.equals(
            true,
        );
    });
    it("Complex or null or null object check (null)", () => {
        expect(testCaster.isType(testDataWithNull)).is.equals(true);
    });
    it("Complex or null or null object cast (null)", () => {
        expect(isEqual(testDataWithNull, testCaster.castTo(testDataWithNull))).is.equals(true);
    });
    it("Complex or null or null object check (undefined)", () => {
        expect(testCaster.isType(testDataWithUndefined)).is.equals(true);
    });
    it("Complex or null or null object cast (undefined)", () => {
        expect(isEqual(testDataWithUndefined, testCaster.castTo(testDataWithUndefined))).is.equals(true);
    });
    it("Complex or null or null object check (wrong inner type)", () => {
        expect(testCaster.isType(testDataWithWrongInnerType)).is.equals(false);
    });
    it("Complex or null or null object cast (wrong inner type)", () => {
        let result = true;
        try {
            isEqual(testDataWithWrongInnerType, testCaster.castTo(testDataWithWrongInnerType));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
