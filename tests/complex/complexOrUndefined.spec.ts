import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
    b: boolean;
    n: number;
}

interface ITest {
    s: string;
    b: boolean;
    n: number;
    x: ITestInner | undefined;
    as: string[];
    an: number[];
    ab: boolean[];
}

const testDataWithoutUndefined: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: { s: "test", n: 1, b: false },
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithUndefined: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: undefined,
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testDataWithWrongInnerType: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: "BAD",
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

const testCaster = createCaster<ITest>()
    .addNumber("n")
    .addString("s")
    .addBoolean("b")
    .addCustomOrUndefined("x", testCasterInner)
    .addStringArray("as")
    .addBooleanArray("ab")
    .addNumberArray("an")
    .build();

describe("Complex or undefined object tests", () => {
    it("Complex or undefined object check (not undefined)", () => {
        expect(testCaster.isType(testDataWithoutUndefined)).is.equals(true);
    });
    it("Complex or undefined object cast (not undefined)", () => {
        expect(isEqual(testDataWithoutUndefined, testCaster.castTo(testDataWithoutUndefined))).is.equals(true);
    });
    it("Complex or undefined object check (undefined)", () => {
        expect(testCaster.isType(testDataWithUndefined)).is.equals(true);
    });
    it("Complex or undefined object cast (undefined)", () => {
        expect(isEqual(testDataWithUndefined, testCaster.castTo(testDataWithUndefined))).is.equals(true);
    });
    it("Complex or undefined object check (wrong inner type)", () => {
        expect(testCaster.isType(testDataWithWrongInnerType)).is.equals(false);
    });
    it("Complex or undefined object cast (wrong inner type)", () => {
        let result = true;
        try {
            isEqual(testDataWithWrongInnerType, testCaster.castTo(testDataWithWrongInnerType));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
