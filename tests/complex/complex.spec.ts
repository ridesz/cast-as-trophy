import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
    b: boolean;
    n: number;
}

interface ITest {
    s: string;
    b: boolean;
    n: number;
    x: ITestInner;
    as: string[];
    an: number[];
    ab: boolean[];
}

const testData: unknown = {
    s: "test",
    n: 1,
    b: false,
    x: { s: "test", n: 1, b: false },
    as: ["test"],
    ab: [true, false],
    an: [12, 23],
};

describe("Complex object tests", () => {
    it("Complex object check", () => {
        const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

        const testCaster = createCaster<ITest>()
            .addNumber("n")
            .addString("s")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addStringArray("as")
            .addBooleanArray("ab")
            .addNumberArray("an")
            .build();
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Complex object cast", () => {
        const testCasterInner = createCaster<ITestInner>().addNumber("n").addString("s").addBoolean("b").build();

        const testCaster = createCaster<ITest>()
            .addNumber("n")
            .addString("s")
            .addBoolean("b")
            .addCustom("x", testCasterInner)
            .addStringArray("as")
            .addBooleanArray("ab")
            .addNumberArray("an")
            .build();
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
});
