import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Complex object array build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITestInner {" +
            "    s: string;" +
            "}" +
            "interface ITest {" +
            "    s: string;" +
            "    x: ITestInner[];" +
            "}" +
            "const testCasterInner = createCaster<ITestInner>()" +
            '    .addString("s")' +
            ".build();" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addCustomArray("x", testCasterInner)' +
            "    .build();";
        testGoodCode(code);
    });
    it("Custom is not an array", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITestInner {" +
            "    s: string;" +
            "}" +
            "interface ITest {" +
            "    s: string;" +
            "    x: ITestInner;" +
            "}" +
            "const testCasterInner = createCaster<ITestInner>()" +
            '    .addString("s")' +
            ".build();" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addCustomArray("x", testCasterInner)' +
            "    .build();";
        testBadCode(code);
    });
    it("Not array checked for a custom array field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITestInner {" +
            "    s: string;" +
            "}" +
            "interface ITest {" +
            "    s: string;" +
            "    x: ITestInner[];" +
            "}" +
            "const testCasterInner = createCaster<ITestInner>()" +
            '    .addString("s")' +
            ".build();" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addCustom("x", testCasterInner)' +
            "    .build();";
        testBadCode(code);
    });
    it("Custom union is not enabled", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITestInner1 {" +
            "    s: string;" +
            "}" +
            "interface ITestInner2 {" +
            "    n: number;" +
            "}" +
            "interface ITest {" +
            "    s: string;" +
            "    x: ITestInner1[] | ITestInner2[];" +
            "}" +
            "const testCasterInner = createCaster<ITestInner1>()" +
            '    .addString("s")' +
            ".build();" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addCustomArray("x", testCasterInner)' +
            "    .build();";
        testBadCode(code);
    });
});
