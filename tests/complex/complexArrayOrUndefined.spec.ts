import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
}

interface ITest {
    s: string;
    x: ITestInner[] | undefined;
}

const testDataWithoutUndefined: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: "test" }],
};

const testDataWithUndefined: unknown = {
    s: "test",
    x: undefined,
};

const wrongTestDataWithWrongArrayElementType: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: 5 }],
};

const wrongTestDataWithUndefinedElement: unknown = {
    s: "test",
    x: [undefined, { s: "test" }],
};

const testCasterInner = createCaster<ITestInner>().addString("s").build();

const testCaster = createCaster<ITest>().addString("s").addCustomArrayOrUndefined("x", testCasterInner).build();

describe("Complex object array or undefined tests", () => {
    it("Complex object array check without undefined", () => {
        expect(testCaster.isType(testDataWithoutUndefined)).is.equals(true);
    });
    it("Complex object array cast without undefined", () => {
        expect(isEqual(testDataWithoutUndefined, testCaster.castTo(testDataWithoutUndefined))).is.equals(true);
    });
    it("Complex object array check with undefined", () => {
        expect(testCaster.isType(testDataWithUndefined)).is.equals(true);
    });
    it("Complex object array cast with undefined", () => {
        expect(isEqual(testDataWithUndefined, testCaster.castTo(testDataWithUndefined))).is.equals(true);
    });
    it("Bad complex object array check (wrong array element type)", () => {
        expect(testCaster.isType(wrongTestDataWithWrongArrayElementType)).is.equals(false);
    });
    it("Bad complex object array cast (wrong array element type)", () => {
        let result = true;
        try {
            isEqual(wrongTestDataWithWrongArrayElementType, testCaster.castTo(wrongTestDataWithWrongArrayElementType));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Bad complex object array check (undefined array element type)", () => {
        expect(testCaster.isType(wrongTestDataWithUndefinedElement)).is.equals(false);
    });
    it("Bad complex object array cast (undefined array element type)", () => {
        let result = true;
        try {
            isEqual(wrongTestDataWithUndefinedElement, testCaster.castTo(wrongTestDataWithUndefinedElement));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
