import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
}

interface ITest {
    s: string;
    x: ITestInner[] | null;
}

const testDataWithoutNull: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: "test" }],
};

const testDataWithNull: unknown = {
    s: "test",
    x: null,
};

const wrongTestDataWithWrongArrayElementType: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: 5 }],
};

const wrongTestDataWithNullElement: unknown = {
    s: "test",
    x: [null, { s: "test" }],
};

const testCasterInner = createCaster<ITestInner>().addString("s").build();

const testCaster = createCaster<ITest>().addString("s").addCustomArrayOrNull("x", testCasterInner).build();

describe("Complex object array or null tests", () => {
    it("Complex object array check without null", () => {
        expect(testCaster.isType(testDataWithoutNull)).is.equals(true);
    });
    it("Complex object array cast without null", () => {
        expect(isEqual(testDataWithoutNull, testCaster.castTo(testDataWithoutNull))).is.equals(true);
    });
    it("Complex object array check with null", () => {
        expect(testCaster.isType(testDataWithNull)).is.equals(true);
    });
    it("Complex object array cast with null", () => {
        expect(isEqual(testDataWithNull, testCaster.castTo(testDataWithNull))).is.equals(true);
    });
    it("Bad complex object array check (wrong array element type)", () => {
        expect(testCaster.isType(wrongTestDataWithWrongArrayElementType)).is.equals(false);
    });
    it("Bad complex object array cast (wrong array element type)", () => {
        let result = true;
        try {
            isEqual(wrongTestDataWithWrongArrayElementType, testCaster.castTo(wrongTestDataWithWrongArrayElementType));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Bad complex object array check (null array element type)", () => {
        expect(testCaster.isType(wrongTestDataWithNullElement)).is.equals(false);
    });
    it("Bad complex object array cast (null array element type)", () => {
        let result = true;
        try {
            isEqual(wrongTestDataWithNullElement, testCaster.castTo(wrongTestDataWithNullElement));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
