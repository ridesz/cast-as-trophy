import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Complex object build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: ITest1;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustom("x", testCaster1)' +
            "    .build();";
        testGoodCode(code);
    });
    it("Custom caster type is wrong", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    b: boolean;" +
            "}" +
            "interface ITest3 {" +
            "    x: ITest1;" +
            "}" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addBoolean("b")' +
            "    .build();" +
            "const testCaster3 = createCaster<ITest3>()" +
            '    .addCustom("x", testCaster2)' +
            "    .build();";
        testBadCode(code);
    });
    it("Custom field is a union and it is forbidden", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    b: boolean;" +
            "}" +
            "interface ITest3 {" +
            "    x: ITest1 | ITest2;" +
            "}" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addBoolean("b")' +
            "    .build();" +
            "const testCaster3 = createCaster<ITest3>()" +
            '    .addCustom("x", testCaster2)' +
            "    .build();";
        testBadCode(code);
    });
});
