import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
}

interface ITest {
    s: string;
    x: ITestInner[] | null | undefined;
}

const testDataWithoutNullOrUndefined: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: "test" }],
};

const testDataWithNull: unknown = {
    s: "test",
    x: null,
};

const testDataWithUndefined: unknown = {
    s: "test",
    x: null,
};

const testDataWithWrong: unknown = {
    s: "test",
    x: [{ y: "nope" }],
};

const testCasterInner = createCaster<ITestInner>().addString("s").build();

const testCaster = createCaster<ITest>().addString("s").addCustomArrayOrNullOrUndefined("x", testCasterInner).build();

describe("Complex object array or null or undefined tests", () => {
    it("Complex object array check without null or undefined", () => {
        expect(testCaster.isType(testDataWithoutNullOrUndefined)).is.equals(true);
    });
    it("Complex object array cast without null or undefined", () => {
        expect(isEqual(testDataWithoutNullOrUndefined, testCaster.castTo(testDataWithoutNullOrUndefined))).is.equals(
            true,
        );
    });
    it("Complex object array check with null", () => {
        expect(testCaster.isType(testDataWithNull)).is.equals(true);
    });
    it("Complex object array check with undefined", () => {
        expect(testCaster.isType(testDataWithUndefined)).is.equals(true);
    });
    it("Bad complex object array check (wrong array element type)", () => {
        expect(testCaster.isType(testDataWithWrong)).is.equals(false);
    });
});
