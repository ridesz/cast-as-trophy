import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITestInner {
    s: string;
}

interface ITest {
    s: string;
    x: ITestInner[];
}

const testData: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: "test" }],
};

const wrongTestData: unknown = {
    s: "test",
    x: [{ s: "test" }, { s: 5 }],
};

const wrongTestDataArrayIsUndefined: unknown = {
    s: "test",
    x: undefined,
};

const testCasterInner = createCaster<ITestInner>().addString("s").build();

const testCaster = createCaster<ITest>().addString("s").addCustomArray("x", testCasterInner).build();

describe("Complex object array tests", () => {
    it("Complex object array check", () => {
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Complex object array cast", () => {
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Bad complex object array check", () => {
        expect(testCaster.isType(wrongTestData)).is.equals(false);
    });
    it("Bad complex object array cast", () => {
        let result = true;
        try {
            isEqual(wrongTestData, testCaster.castTo(wrongTestData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Bad complex object array check (array is undefined)", () => {
        expect(testCaster.isType(wrongTestDataArrayIsUndefined)).is.equals(false);
    });
    it("Bad complex object array cast (array is undefined)", () => {
        let result = true;
        try {
            isEqual(wrongTestDataArrayIsUndefined, testCaster.castTo(wrongTestDataArrayIsUndefined));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
