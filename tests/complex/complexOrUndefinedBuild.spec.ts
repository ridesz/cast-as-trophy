import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Complex object or undefined build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: ITest1 | undefined;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testGoodCode(code);
    });
    it("The ? is OK instead of the undefined", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x?: ITest1;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testGoodCode(code);
    });
    it("Undfined is not OK for the addCustom", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: ITest1 | undefined;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustom("x", testCaster1)' +
            "    .build();";
        testBadCode(code);
    });
    it("Not undefined is not OK for addCustomOrUndefined", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: ITest1;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testBadCode(code);
    });
    it("Just undefined is not OK for addCustomOrUndefined", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: undefined;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testBadCode(code);
    });
    it("Just custom caster is not OK for addCustomOrUndefined", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    x: ITest1;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest2>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testBadCode(code);
    });
    it("Custom type union is not OK for addCustomOrUndefined", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest1 {" +
            "    s: string;" +
            "}" +
            "interface ITest2 {" +
            "    n: number;" +
            "}" +
            "interface ITest3 {" +
            "    x: ITest1 | ITest2 | undefined;" +
            "}" +
            "const testCaster1 = createCaster<ITest1>()" +
            '    .addString("s")' +
            "    .build();" +
            "const testCaster2 = createCaster<ITest3>()" +
            '    .addCustomOrUndefined("x", testCaster1)' +
            "    .build();";
        testBadCode(code);
    });
});
