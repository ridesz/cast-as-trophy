import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    na: number[];
}

const testCaster = createCaster<ITest>().addNumberArray("na").build();

describe("Number array caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            na: [1, 2],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value", () => {
        const testData: unknown = {
            na: [1, 2],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            na: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            na: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            na: ["test", 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            na: ["test", 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
