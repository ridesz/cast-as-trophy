import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Number array  or undefined caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: number[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArrayOrUndefined("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Simple addNumberArray is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: number[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArray("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("The addNumberArrayOrUndefined for type without undefined is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: number[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArrayOrUndefined("s")' +
            "    .build();";
        testBadCode(code);
    });
});
