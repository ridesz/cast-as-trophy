import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    nu: number | undefined;
}

const testCaster = createCaster<ITest>().addNumberOrUndefined("nu").build();

describe("Number or undefined caster tests", () => {
    it("Check good number value", () => {
        const testData: unknown = {
            nu: 42,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good number value", () => {
        const testData: unknown = {
            nu: 42,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good undefined value", () => {
        const testData: unknown = {
            nu: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good undefined value", () => {
        const testData: unknown = {
            nu: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            nu: "test",
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            nu: "test",
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
