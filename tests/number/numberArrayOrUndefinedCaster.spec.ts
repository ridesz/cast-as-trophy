import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    nau: number[] | undefined;
}

const testCaster = createCaster<ITest>().addNumberArrayOrUndefined("nau").build();

describe("Number array or undefined caster tests", () => {
    it("Check good value (not undefined)", () => {
        const testData: unknown = {
            nau: [1, 2],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (not undefined)", () => {
        const testData: unknown = {
            nau: [1, 2],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good value (undefined)", () => {
        const testData: unknown = {
            nau: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (undefined)", () => {
        const testData: unknown = {
            nau: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });

    it("Check bad value", () => {
        const testData: unknown = {
            nau: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            nau: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            nau: ["test", 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            nau: ["test", 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
