import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    n: number;
}

const testCaster = createCaster<ITest>().addNumber("n").build();

describe("Number caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            n: 42,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value", () => {
        const testData: unknown = {
            n: 42,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            n: "test",
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            n: "test",
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
