import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Number caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            '    .addNumber("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testBadCode(code);
    });
    it("Number literal is not enabled (single item, marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: 0;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testBadCode(code);
    });
    it("Number literal is not enabled (single item, not marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: 99;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testBadCode(code);
    });
    it("Number literal is not enabled (union with marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: 0 | 99;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testBadCode(code);
    });
    it("Number literal is not enabled (union without marker)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: 1 | 2;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumber("n")' +
            "    .build();";
        testBadCode(code);
    });
});
