import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Number array caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArray("n")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: number[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArray("n")' +
            '    .addNumberArray("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    n: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addNumberArray("n")' +
            "    .build();";
        testBadCode(code);
    });
});
