import "mocha";

import { testGoodCode, testBadCode } from "../helper";

const typeInfos = [
    ["s", "string", "String", '"OK"'],
    ["n", "number", "Number", "0"],
    ["b", "boolean", "Boolean", "true"],
];

describe("Null or undefined array casters build tests", () => {
    for (const typeInfo of typeInfos) {
        const [first, typeName, funcName, literal] = typeInfo;
        it(`Successful build for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: Array<${typeName}> | null | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}ArrayOrNullOrUndefined("${first}")` +
                "    .build();";
            testGoodCode(code);
        });
        it(`Original method should not enable null or undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: Array<${typeName}> | null | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Null or undefined method when only null or undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: null | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}ArrayOrNullOrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Null or undefined method when no null or undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: Array<${typeName}>;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}ArrayOrNullOrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Literal is not enabled on null or undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: Array<${literal}> | null | null;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}ArrayOrNullOrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
    }
});
