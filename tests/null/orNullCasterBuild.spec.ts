import "mocha";

import { testGoodCode, testBadCode } from "../helper";

const typeInfos = [
    ["s", "string", "String", '"OK"'],
    ["n", "number", "Number", "0"],
    ["b", "boolean", "Boolean", "true"],
];

describe("Null casters build tests", () => {
    for (const typeInfo of typeInfos) {
        const [first, typeName, funcName, literal] = typeInfo;
        it(`Successful build for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName} | null;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrNull("${first}")` +
                "    .build();";
            testGoodCode(code);
        });
        it(`Original method should not enable null for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName} | null;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Null method when only null for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: null;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrNull("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Null method when no null for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName};` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrNull("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Literal is not enabled on null for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${literal} | null;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrNull("${first}")` +
                "    .build();";
            testBadCode(code);
        });
    }
});
