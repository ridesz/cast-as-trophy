import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../src";

interface ITest {
    b: Array<boolean>;
}

const testCaster = createCaster<ITest>().addBooleanArray("b").build();

describe("Copied tests", () => {
    it("Cast to", () => {
        const testData: ITest = {
            b: [true],
        };
        const result = testCaster.castTo(testData);
        testData.b.push(true);
        expect(isEqual(result.b.length, testData.b.length)).is.equals(true);
    });
    it("Copy to", () => {
        const testData: ITest = {
            b: [true],
        };
        const result = testCaster.copyTo(testData);
        testData.b.push(true);
        expect(!isEqual(result.b.length, testData.b.length)).is.equals(true);
    });
    it("Async cast to", async () => {
        const testData: ITest = {
            b: [true],
        };
        const result = await testCaster.asyncCastTo(testData);
        testData.b.push(true);
        expect(isEqual(result.b.length, testData.b.length)).is.equals(true);
    });
    it("Async cast to", async () => {
        const testData: ITest = {
            b: [true],
        };
        const result = await testCaster.asyncCopyTo(testData);
        testData.b.push(true);
        expect(!isEqual(result.b.length, testData.b.length)).is.equals(true);
    });
});
