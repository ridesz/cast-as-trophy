import "mocha";

import { testGoodCode, testBadCode } from "./helper";

describe("Same field again test", () => {
    it("Field added only once", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Same field again is forbidden", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addString("s")' +
            '    .addString("s")' +
            "    .build();";
        testBadCode(code);
    });
});
