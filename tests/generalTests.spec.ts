import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../src";
import { Caster } from "../src/caster";

interface IEmbedded {
    s: string;
    n: number;
    b: boolean;
}

const embeddedCaster = createCaster<IEmbedded>().addString("s").addNumber("n").addBoolean("b").build();

const GOOD_STRING = "test";
const GOOD_NUMBER = 42;
const GOOD_BOOLEAN = true;
const GOOD_CUSTOM = { s: "test", n: 42, b: true };
const GOOD_STRING_ARRAY = [GOOD_STRING, GOOD_STRING];
const GOOD_NUMBER_ARRAY = [GOOD_NUMBER, GOOD_NUMBER];
const GOOD_BOOLEAN_ARRAY = [GOOD_BOOLEAN, GOOD_BOOLEAN];
const GOOD_CUSTOM_ARRAY = [GOOD_CUSTOM, GOOD_CUSTOM];

const badValues = [[GOOD_STRING, GOOD_NUMBER], { x: 13, y: "nope", z: false }, [undefined], [null]];

const configs: Array<[string, Caster<unknown>, Array<unknown>]> = [
    // Basic
    ["String", createCaster<{ x: string }>().addString("x").build(), [GOOD_STRING]],
    ["Number", createCaster<{ x: number }>().addNumber("x").build(), [GOOD_NUMBER]],
    ["Boolean", createCaster<{ x: boolean }>().addBoolean("x").build(), [GOOD_BOOLEAN]],
    ["Custom", createCaster<{ x: IEmbedded }>().addCustom("x", embeddedCaster).build(), [GOOD_CUSTOM]],

    // Undefined
    [
        "String or undefined",
        createCaster<{ x: string | undefined }>().addStringOrUndefined("x").build(),
        [GOOD_STRING, undefined],
    ],
    [
        "Number or undefined",
        createCaster<{ x: number | undefined }>().addNumberOrUndefined("x").build(),
        [GOOD_NUMBER, undefined],
    ],
    [
        "Boolean or undefined",
        createCaster<{ x: boolean | undefined }>().addBooleanOrUndefined("x").build(),
        [GOOD_BOOLEAN, undefined],
    ],
    [
        "Custom or undefined",
        createCaster<{ x: IEmbedded | undefined }>().addCustomOrUndefined("x", embeddedCaster).build(),
        [GOOD_CUSTOM, undefined],
    ],

    // Null
    ["String or null", createCaster<{ x: string | null }>().addStringOrNull("x").build(), [GOOD_STRING, null]],
    ["Number or null", createCaster<{ x: number | null }>().addNumberOrNull("x").build(), [GOOD_NUMBER, null]],
    ["Boolean or null", createCaster<{ x: boolean | null }>().addBooleanOrNull("x").build(), [GOOD_BOOLEAN, null]],
    [
        "Custom or null",
        createCaster<{ x: IEmbedded | null }>().addCustomOrNull("x", embeddedCaster).build(),
        [GOOD_CUSTOM, null],
    ],

    // Null or undefined
    [
        "String or null or undefined",
        createCaster<{ x: string | null | undefined }>().addStringOrNullOrUndefined("x").build(),
        [GOOD_STRING, null, undefined],
    ],
    [
        "Number or null or undefined",
        createCaster<{ x: number | null | undefined }>().addNumberOrNullOrUndefined("x").build(),
        [GOOD_NUMBER, null, undefined],
    ],
    [
        "Boolean or null or undefined",
        createCaster<{ x: boolean | null | undefined }>().addBooleanOrNullOrUndefined("x").build(),
        [GOOD_BOOLEAN, null, undefined],
    ],
    [
        "Custom or null or undefined",
        createCaster<{ x: IEmbedded | null | undefined }>().addCustomOrNullOrUndefined("x", embeddedCaster).build(),
        [GOOD_CUSTOM, null, undefined],
    ],

    // Basic array
    ["String array", createCaster<{ x: Array<string> }>().addStringArray("x").build(), [GOOD_STRING_ARRAY]],
    ["Number array", createCaster<{ x: Array<number> }>().addNumberArray("x").build(), [GOOD_NUMBER_ARRAY]],
    ["Boolean array", createCaster<{ x: Array<boolean> }>().addBooleanArray("x").build(), [GOOD_BOOLEAN_ARRAY]],
    [
        "Custom array",
        createCaster<{ x: Array<IEmbedded> }>().addCustomArray("x", embeddedCaster).build(),
        [GOOD_CUSTOM_ARRAY],
    ],

    // Array or undefined
    [
        "String array or undefined",
        createCaster<{ x: Array<string> | undefined }>().addStringArrayOrUndefined("x").build(),
        [GOOD_STRING_ARRAY, undefined],
    ],
    [
        "Number array or undefined",
        createCaster<{ x: Array<number> | undefined }>().addNumberArrayOrUndefined("x").build(),
        [GOOD_NUMBER_ARRAY, undefined],
    ],
    [
        "Boolean array or undefined",
        createCaster<{ x: Array<boolean> | undefined }>().addBooleanArrayOrUndefined("x").build(),
        [GOOD_BOOLEAN_ARRAY, undefined],
    ],
    [
        "Custom array or undefined",
        createCaster<{ x: Array<IEmbedded> | undefined }>().addCustomArrayOrUndefined("x", embeddedCaster).build(),
        [GOOD_CUSTOM_ARRAY, undefined],
    ],

    // Array or null
    [
        "String array or null",
        createCaster<{ x: Array<string> | null }>().addStringArrayOrNull("x").build(),
        [GOOD_STRING_ARRAY, null],
    ],
    [
        "Number array or null",
        createCaster<{ x: Array<number> | null }>().addNumberArrayOrNull("x").build(),
        [GOOD_NUMBER_ARRAY, null],
    ],
    [
        "Boolean array or null",
        createCaster<{ x: Array<boolean> | null }>().addBooleanArrayOrNull("x").build(),
        [GOOD_BOOLEAN_ARRAY, null],
    ],
    [
        "Custom or null",
        createCaster<{ x: Array<IEmbedded> | null }>().addCustomArrayOrNull("x", embeddedCaster).build(),
        [GOOD_CUSTOM_ARRAY, null],
    ],

    // Array or null or undefined
    [
        "String array or null or undefined",
        createCaster<{ x: Array<string> | null | undefined }>().addStringArrayOrNullOrUndefined("x").build(),
        [GOOD_STRING_ARRAY, null, undefined],
    ],
    [
        "Number array or null or undefined",
        createCaster<{ x: Array<number> | null | undefined }>().addNumberArrayOrNullOrUndefined("x").build(),
        [GOOD_NUMBER_ARRAY, null, undefined],
    ],
    [
        "Boolean array or null or undefined",
        createCaster<{ x: Array<boolean> | null | undefined }>().addBooleanArrayOrNullOrUndefined("x").build(),
        [GOOD_BOOLEAN_ARRAY, null, undefined],
    ],
    [
        "Custom array or null or undefined",
        createCaster<{ x: Array<IEmbedded> | null | undefined }>()
            .addCustomArrayOrNullOrUndefined("x", embeddedCaster)
            .build(),
        [GOOD_CUSTOM_ARRAY, null, undefined],
    ],
];

const allValueSet = new Set<unknown>();
badValues.forEach((badValue) => {
    allValueSet.add(badValue);
});
configs.forEach((config) => {
    const values = config[2];
    values.forEach((value) => {
        allValueSet.add(value);
    });
});
const allValues = Array.from(allValueSet).sort();

configs.forEach((config) => {
    const [name, caster, goodTestValueArray] = config;
    const goodValues = new Set<unknown>();
    goodTestValueArray.forEach((goodTestValue) => {
        goodValues.add(goodTestValue);
    });
    describe(`General test: ${name}`, () => {
        allValues.forEach((value) => {
            // ...
            const testData: unknown = {
                x: value,
            };
            const isGood = goodValues.has(value);

            // Is type
            it(`isType(...) -> ${name} -> ${JSON.stringify(value)} -> ${isGood ? "GOOD" : "BAD"}?`, () => {
                expect(caster.isType(testData)).is.equals(isGood);
            });

            // Cast to
            it(`castTo(...) -> ${name} -> ${JSON.stringify(value)} -> ${isGood ? "GOOD" : "BAD"}?`, () => {
                let result = !isGood;
                try {
                    result = isEqual(testData, caster.castTo(testData));
                } catch (error) {
                    result = false;
                }
                expect(result).is.equals(isGood);
            });

            // Copy to
            it(`copyTo(...) -> ${name} -> ${JSON.stringify(value)} -> ${isGood ? "GOOD" : "BAD"}?`, () => {
                let result = !isGood;
                try {
                    result = isEqual(testData, caster.copyTo(testData));
                } catch {
                    result = false;
                }
                expect(result).is.equals(isGood);
            });

            // Async cast to
            it(`asyncCastTo(...) -> ${name} -> ${JSON.stringify(value)} -> ${isGood ? "GOOD" : "BAD"}?`, async () => {
                let result = !isGood;
                try {
                    result = isEqual(testData, await caster.asyncCastTo(testData));
                } catch {
                    result = false;
                }
                expect(result).is.equals(isGood);
            });

            // Copy to
            it(`asyncCopyTo(...) -> ${name} -> ${JSON.stringify(value)} -> ${isGood ? "GOOD" : "BAD"}?`, async () => {
                let result = !isGood;
                try {
                    result = isEqual(testData, await caster.asyncCopyTo(testData));
                } catch {
                    result = false;
                }
                expect(result).is.equals(isGood);
            });
        });
    });
});
