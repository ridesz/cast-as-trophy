import "mocha";

import { testGoodCode, testBadCode } from "./helper";

const values = [
    ["string", "addString"],
    ["number", "addNumber"],
    ["boolean", "addBoolean"],
    ["string | undefined", "addStringOrUndefined"],
    ["number | undefined", "addNumberOrUndefined"],
    ["boolean | undefined", "addBooleanOrUndefined"],
    ["string | null", "addStringOrNull"],
    ["number | null", "addNumberOrNull"],
    ["boolean | null", "addBooleanOrNull"],
    ["string | null | undefined", "addStringOrNullOrUndefined"],
    ["number | null | undefined", "addNumberOrNullOrUndefined"],
    ["boolean | null | undefined", "addBooleanOrNullOrUndefined"],

    ["Array<string>", "addStringArray"],
    ["Array<number>", "addNumberArray"],
    ["Array<boolean>", "addBooleanArray"],
    ["Array<string> | undefined", "addStringArrayOrUndefined"],
    ["Array<number> | undefined", "addNumberArrayOrUndefined"],
    ["Array<boolean> | undefined", "addBooleanArrayOrUndefined"],
    ["Array<string> | null", "addStringArrayOrNull"],
    ["Array<number> | null", "addNumberArrayOrNull"],
    ["Array<boolean> | null", "addBooleanArrayOrNull"],
    ["Array<string> | null | undefined", "addStringArrayOrNullOrUndefined"],
    ["Array<number> | null | undefined", "addNumberArrayOrNullOrUndefined"],
    ["Array<boolean> | null | undefined", "addBooleanArrayOrNullOrUndefined"],

    ["IEmbedded", "addCustom"],
    ["IEmbedded | undefined", "addCustomOrUndefined"],
    ["IEmbedded | null", "addCustomOrNull"],
    ["IEmbedded | null | undefined", "addCustomOrNullOrUndefined"],
    ["Array<IEmbedded>", "addCustomArray"],
    ["Array<IEmbedded> | undefined", "addCustomArrayOrUndefined"],
    ["Array<IEmbedded> | null", "addCustomArrayOrNull"],
    ["Array<IEmbedded> | null | undefined", "addCustomArrayOrNullOrUndefined"],
];

values.forEach((outer, outerIndex) => {
    const methodName = outer[1];
    const secondParam = methodName.includes("addCustom") ? ", embeddedCaster" : "";
    describe(`Caster "${methodName}(...)" add methods test`, () => {
        values.forEach((inner, innerIndex) => {
            const theType = inner[0];
            it(`The "${methodName}(...)" method for a "${theType}" type`, () => {
                const code =
                    'import { createCaster } from "./src";' +
                    "interface IEmbedded {" +
                    "    s: string;" +
                    "}" +
                    "const embeddedCaster = createCaster<IEmbedded>()" +
                    '    .addString("s")' +
                    "    .build();" +
                    "interface ITest {" +
                    `    x: ${theType};` +
                    "}" +
                    "const testCaster = createCaster<ITest>()" +
                    `    .${methodName}("x" ${secondParam} )` +
                    "    .build();";
                if (outerIndex === innerIndex) {
                    testGoodCode(code);
                } else {
                    testBadCode(code);
                }
            });
        });
    });
});
