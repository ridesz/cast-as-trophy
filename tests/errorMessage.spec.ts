import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../src";

interface ITest {
    b: boolean;
}

const testCaster = createCaster<ITest>().addBoolean("b").build();

describe("Error message tests", () => {
    it("Type check", () => {
        const testData: unknown = {
            b: 42,
        };
        let result = false;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch (error) {
            if (error instanceof Error) {
                if (error.message === "b -> This value is not a boolean: 42") {
                }
            }
            result = false;
        }
        expect(result).is.equals(false);
    });
});
