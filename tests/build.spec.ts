import "mocha";

import { testGoodCode, testBadCode } from "./helper";

describe("Build test", () => {
    it("Node-ts with good code", () => {
        testGoodCode('true ? undefined : console.log("Hello, world!");');
    });
    it("Node-ts with bad code", () => {
        testBadCode('This is not a valid code..."');
    });
});
