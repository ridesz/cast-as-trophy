import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../src";

interface ITest {
    s: string;
    b: boolean;
    n: number;
}

const testData: unknown = {
    s: "test",
    n: 1,
    b: false,
};

const testCaster = createCaster<ITest>().addNumber("n").addString("s").addBoolean("b").build();

function simpleObjectType(): void {
    expect(testCaster.isType(testData)).is.equals(true);
}

function simpleObjectCast(): void {
    expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
}

function simpleObjectCopy(): void {
    expect(isEqual(testData, testCaster.copyTo(testData))).is.equals(true);
}

async function simpleObjectAsyncCast(): Promise<void> {
    expect(isEqual(testData, await testCaster.asyncCastTo(testData))).is.equals(true);
}

async function simpleObjectAsyncCopy(): Promise<void> {
    expect(isEqual(testData, await testCaster.asyncCopyTo(testData))).is.equals(true);
}

describe("Simple object tests", () => {
    it("Type check", simpleObjectType);
    it("Cast", simpleObjectCast);
    it("Copy", simpleObjectCopy);
    it("Async cast", simpleObjectAsyncCast);
    it("Async copy", simpleObjectAsyncCopy);
});
