import { expect } from "chai";
import "mocha";
import * as shellJs from "shelljs";

function testCode(code: string): boolean {
    let errors = false;
    const result = shellJs.exec("ts-node -e '" + code + "'", { silent: true });
    if (result.code !== 0) {
        errors = true;
    }
    return errors;
}

export function testBadCode(code: string): void {
    expect(testCode(code)).is.equals(true);
}

export function testGoodCode(code: string): void {
    expect(testCode(code)).is.equals(false);
}
