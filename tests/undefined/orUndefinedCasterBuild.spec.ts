import "mocha";

import { testGoodCode, testBadCode } from "../helper";

const typeInfos = [
    ["s", "string", "String", '"OK"'],
    ["n", "number", "Number", "0"],
    ["b", "boolean", "Boolean", "true"],
];

describe("Undefined casters build tests", () => {
    for (const typeInfo of typeInfos) {
        const [first, typeName, funcName, literal] = typeInfo;
        it(`Successful build for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName} | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrUndefined("${first}")` +
                "    .build();";
            testGoodCode(code);
        });
        it(`Original method should not enable undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName} | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Undefined method when only undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Undefined method when no undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${typeName};` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
        it(`Literal is not enabled on undefined for ${typeName}`, () => {
            const code =
                'import { createCaster } from "./src";' +
                "interface ITest {" +
                `    ${first}: ${literal} | undefined;` +
                "}" +
                "const testCaster = createCaster<ITest>()" +
                `    .add${funcName}OrUndefined("${first}")` +
                "    .build();";
            testBadCode(code);
        });
    }
});
