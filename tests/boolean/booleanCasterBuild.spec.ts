import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Boolean caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            '    .addBoolean("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: string;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            "    .build();";
        testBadCode(code);
    });
    it("Boolean literal is not supported (true)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: true;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            "    .build();";
        testBadCode(code);
    });
    it("Boolean literal is not supported (false)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: false;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            "    .build();";
        testBadCode(code);
    });
    it("The boolean literal true | false is fine because it is the boolean itself (in practice)", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: true | false;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBoolean("b")' +
            "    .build();";
        testGoodCode(code);
    });
});
