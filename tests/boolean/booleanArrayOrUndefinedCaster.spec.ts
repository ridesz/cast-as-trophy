import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    x: boolean[] | undefined;
}

const testCaster = createCaster<ITest>().addBooleanArrayOrUndefined("x").build();

describe("Boolean array or undefined caster tests", () => {
    it("Check good value (not undefined)", () => {
        const testData: unknown = {
            x: [true, false],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (not undefined)", () => {
        const testData: unknown = {
            x: [true, false],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good value (undefined)", () => {
        const testData: unknown = {
            x: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (undefined)", () => {
        const testData: unknown = {
            x: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });

    it("Check bad value", () => {
        const testData: unknown = {
            x: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            x: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            x: [true, 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            x: [true, 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Cast bad value (undefined in array)", () => {
        const testData: unknown = {
            x: [true, undefined],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
