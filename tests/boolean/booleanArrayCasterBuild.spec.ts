import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Boolean array caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArray("b")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Missing field in the caster", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            "    .build();";
        testBadCode(code);
    });
    it("Not existing field", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: boolean[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArray("b")' +
            '    .addBooleanArray("x")' +
            "    .build();";
        testBadCode(code);
    });
    it("Bad field type", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    b: string[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArray("b")' +
            "    .build();";
        testBadCode(code);
    });
});
