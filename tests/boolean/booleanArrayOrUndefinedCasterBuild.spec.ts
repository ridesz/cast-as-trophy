import "mocha";

import { testGoodCode, testBadCode } from "../helper";

describe("Boolean array  or undefined caster build tests", () => {
    it("Successful build", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: boolean[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArrayOrUndefined("s")' +
            "    .build();";
        testGoodCode(code);
    });
    it("Simple addBooleanArray is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: boolean[] | undefined;" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArray("s")' +
            "    .build();";
        testBadCode(code);
    });
    it("The addNumberBooleanOrUndefined for type without undefined is not OK", () => {
        const code =
            'import { createCaster } from "./src";' +
            "interface ITest {" +
            "    s: boolean[];" +
            "}" +
            "const testCaster = createCaster<ITest>()" +
            '    .addBooleanArrayOrUndefined("s")' +
            "    .build();";
        testBadCode(code);
    });
});
