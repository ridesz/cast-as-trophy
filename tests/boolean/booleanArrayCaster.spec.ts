import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    ba: boolean[];
}

const testCaster = createCaster<ITest>().addBooleanArray("ba").build();

describe("Boolean array caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            ba: [true, false],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });

    it("Cast good value", () => {
        const testData: unknown = {
            ba: [true, false],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });

    it("Check bad value (not array)", () => {
        const testData: unknown = {
            ba: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });

    it("Cast bad value (not array)", () => {
        const testData: unknown = {
            ba: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Cast bad value (not array) (async)", async () => {
        const testData: unknown = {
            ba: 42,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCastTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            ba: ["test", 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });

    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            ba: ["test", 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
