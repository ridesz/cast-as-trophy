import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    b: boolean;
}

const testCaster = createCaster<ITest>().addBoolean("b").build();

describe("Boolean caster tests", () => {
    it("Check good value", () => {
        const testData: unknown = {
            b: true,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value", () => {
        const testData: unknown = {
            b: true,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            b: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            b: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value", () => {
        const testData: unknown = {
            b: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async cast bad value", async () => {
        const testData: unknown = {
            b: 42,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async copy bad value", async () => {
        const testData: unknown = {
            b: 42,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    // Null
    it("Check bad value (null)", () => {
        const testData: unknown = {
            b: null,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (null)", () => {
        const testData: unknown = {
            b: null,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value (null)", () => {
        const testData: unknown = {
            b: null,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async cast bad value (null)", async () => {
        const testData: unknown = {
            b: null,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async copy bad value (null)", async () => {
        const testData: unknown = {
            b: null,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    // Undefined
    it("Check bad value (undefined)", () => {
        const testData: unknown = {
            b: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (undefined)", () => {
        const testData: unknown = {
            b: undefined,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value (undefined)", () => {
        const testData: unknown = {
            b: undefined,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async cast bad value (undefined)", async () => {
        const testData: unknown = {
            b: undefined,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async copy bad value (undefined)", async () => {
        const testData: unknown = {
            b: undefined,
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
