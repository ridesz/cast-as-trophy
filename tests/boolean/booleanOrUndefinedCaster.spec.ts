import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    bu: boolean | undefined;
}

const testCaster = createCaster<ITest>().addBooleanOrUndefined("bu").build();

describe("Boolean or undefined caster tests", () => {
    it("Check good boolean value", () => {
        const testData: unknown = {
            bu: true,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good number value", () => {
        const testData: unknown = {
            bu: true,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good undefined value", () => {
        const testData: unknown = {
            bu: undefined,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good undefined value", () => {
        const testData: unknown = {
            bu: undefined,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check bad value", () => {
        const testData: unknown = {
            bu: "test",
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            bu: "test",
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value", () => {
        const testData: unknown = {
            bu: "test",
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async cast bad value", async () => {
        const testData: unknown = {
            bu: "test",
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCastTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async copy bad value", async () => {
        const testData: unknown = {
            bu: "test",
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
