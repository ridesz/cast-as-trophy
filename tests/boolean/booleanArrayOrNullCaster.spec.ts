import { expect } from "chai";
import "mocha";
import { isEqual } from "lodash";

import { createCaster } from "../../src";

interface ITest {
    x: boolean[] | null;
}

const testCaster = createCaster<ITest>().addBooleanArrayOrNull("x").build();

describe("Boolean array or null caster tests", () => {
    it("Check good value (not null)", () => {
        const testData: unknown = {
            x: [true, false],
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (not null)", () => {
        const testData: unknown = {
            x: [true, false],
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });
    it("Check good value (null)", () => {
        const testData: unknown = {
            x: null,
        };
        expect(testCaster.isType(testData)).is.equals(true);
    });
    it("Cast good value (null)", () => {
        const testData: unknown = {
            x: null,
        };
        expect(isEqual(testData, testCaster.castTo(testData))).is.equals(true);
    });

    it("Check bad value", () => {
        const testData: unknown = {
            x: 42,
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value", () => {
        const testData: unknown = {
            x: 42,
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });

    it("Check bad value (bad array)", () => {
        const testData: unknown = {
            x: [true, 42],
        };
        expect(testCaster.isType(testData)).is.equals(false);
    });
    it("Cast bad value (bad array)", () => {
        const testData: unknown = {
            x: [true, 42],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Cast bad value (null in array)", () => {
        const testData: unknown = {
            x: [true, null],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.castTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Copy bad value (null in array)", () => {
        const testData: unknown = {
            x: [true, null],
        };
        let result = true;
        try {
            isEqual(testData, testCaster.copyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async cast bad value (null in array)", async () => {
        const testData: unknown = {
            x: [true, null],
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCastTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
    it("Async copy bad value (null in array)", async () => {
        const testData: unknown = {
            x: [true, null],
        };
        let result = true;
        try {
            isEqual(testData, await testCaster.asyncCopyTo(testData));
        } catch {
            result = false;
        }
        expect(result).is.equals(false);
    });
});
