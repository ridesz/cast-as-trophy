import { DisableNever, DisableNeverArray } from "../never";
import { ExactlyString } from "./exactlyString";

type StringArray<T> = [T] extends [Array<infer ELEMENT>] ? ExactlyString<ELEMENT> : never;

type DisableStringTuple<T> = [T] extends [[string, ...unknown[]]] ? never : unknown;

export type ExactlyStringArray<T> = DisableNever<T> & DisableNeverArray<T> & StringArray<T> & DisableStringTuple<T>;
