import { DisableNever } from "../never";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyString } from "./exactlyString";

export type ExactlyStringOrUndefined<T> = DisableNever<T> & ExactlyString<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
