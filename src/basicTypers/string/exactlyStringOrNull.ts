import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyString } from "./exactlyString";

export type ExactlyStringOrNull<T> = DisableNever<T> & ExactlyString<RemoveNull<T>> & HasNullOrMore<T>;
