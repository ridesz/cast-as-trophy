import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyStringArray } from "./exactlyStringArray";

export type ExactlyStringArrayOrNull<T> = ExactlyStringArray<RemoveNull<T>> & HasNullOrMore<T>;
