import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyStringArray } from "./exactlyStringArray";

export type ExactlyStringArrayOrNullOrUndefined<T> = ExactlyStringArray<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
