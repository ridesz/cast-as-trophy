import { Invert } from "../invert";
import { DisableNever } from "../never";

type NotContainTheMarker<T> = Invert<T extends "OK" ? unknown : never>;
type NotContainNonMarker<T> = Invert<T & "OK" extends never ? unknown : never>;
type NotStringLiteral<T> = NotContainTheMarker<T> & NotContainNonMarker<T>;

type IsString<T> = [T] extends [string] ? unknown : never;

/**
 * String type without union.
 */
export type ExactlyString<T> = DisableNever<T> & NotStringLiteral<T> & IsString<T>;
