import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyString } from "./exactlyString";

export type ExactlyStringOrNullOrUndefined<T> = DisableNever<T> &
    ExactlyString<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
