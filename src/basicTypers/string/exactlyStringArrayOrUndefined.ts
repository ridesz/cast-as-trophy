import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyStringArray } from "./exactlyStringArray";

export type ExactlyStringArrayOrUndefined<T> = ExactlyStringArray<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
