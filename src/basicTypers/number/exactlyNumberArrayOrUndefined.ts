import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyNumberArray } from "./exactlyNumberArray";

export type ExactlyNumberArrayOrUndefined<T> = ExactlyNumberArray<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
