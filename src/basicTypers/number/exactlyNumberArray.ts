import { DisableNever, DisableNeverArray } from "../never";
import { ExactlyNumber } from "./exactlyNumber";

type NumberArray<T> = [T] extends [Array<infer ELEMENT>] ? ExactlyNumber<ELEMENT> : never;

type DisableNumberTuple<T> = [T] extends [[number, ...unknown[]]] ? never : unknown;

export type ExactlyNumberArray<T> = DisableNever<T> & DisableNeverArray<T> & NumberArray<T> & DisableNumberTuple<T>;
