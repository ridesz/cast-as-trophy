import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyNumber } from "./exactlyNumber";

export type ExactlyNumberOrNullOrUndefined<T> = DisableNever<T> &
    ExactlyNumber<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
