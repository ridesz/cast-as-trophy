import { DisableNever } from "../never";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyNumber } from "./exactlyNumber";

export type ExactlyNumberOrUndefined<T> = DisableNever<T> & ExactlyNumber<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
