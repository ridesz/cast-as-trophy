import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyNumberArray } from "./exactlyNumberArray";

export type ExactlyNumberArrayOrNull<T> = ExactlyNumberArray<RemoveNull<T>> & HasNullOrMore<T>;
