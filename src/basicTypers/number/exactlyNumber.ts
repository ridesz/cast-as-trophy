import { Invert } from "../invert";
import { DisableNever } from "../never";

type NotContainTheMarker<T> = Invert<T extends 0 ? unknown : never>;
type NotContainNonMarker<T> = Invert<T & 0 extends never ? unknown : never>;
type NotNumberLiteral<T> = NotContainTheMarker<T> & NotContainNonMarker<T>;

type IsNumber<T> = [T] extends [number] ? unknown : never;

/**
 * Number type without union.
 */
export type ExactlyNumber<T> = DisableNever<T> & NotNumberLiteral<T> & IsNumber<T>;
