import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyNumber } from "./exactlyNumber";

export type ExactlyNumberOrNull<T> = DisableNever<T> & ExactlyNumber<RemoveNull<T>> & HasNullOrMore<T>;
