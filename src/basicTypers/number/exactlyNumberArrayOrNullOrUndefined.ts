import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyNumberArray } from "./exactlyNumberArray";

export type ExactlyNumberArrayOrNullOrUndefined<T> = ExactlyNumberArray<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
