export type Invert<T> = [T] extends [never] ? unknown : never;
