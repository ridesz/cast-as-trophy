export type DisableNever<T> = T extends never ? never : unknown;
