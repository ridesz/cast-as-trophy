export type DisableNeverArray<T> = T extends [Array<never>] ? never : unknown;
