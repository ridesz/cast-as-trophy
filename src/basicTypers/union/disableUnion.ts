import { Invert } from "../invert";

type UnionDisabler<TYPE1, TYPE2> = Invert<TYPE1 extends TYPE2 ? (TYPE2 extends TYPE1 ? never : unknown) : unknown>;

export type DisableUnion<T> = UnionDisabler<T, T>;
