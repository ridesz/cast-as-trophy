# The union killer part. (How to get the enabled primitive types without unions.)

Some notes about the tricks below:

-   The `unknown` type is not a subtype of the `string`, `number` and `boolean`
-   For example: `unknown extends string` will be false
-   The `unknown` type is the supertype of the `string`, `number`, and `boolean`
-   For example: `string | unknown` will be `unknown`
-   The `unknown` type itself is an infectious type: the result will be an `unknown` when you use the `|` with `unknown and with anything else
-   I check the target type and return `unknown` when the current type is not the subtype of the target type
-   I check the other types and return `unknown` when any of them is the super type of the current type
-   The first part will be `unknown` when something is wrong, and the target type when everything is fine
-   When the first part is `unknown` then it is not the subtype of the target type
-   When there is `unknown` from the first part then the final return type will be `never`
-   When the first part returns with the target type then we would like to keep that type as the final return type.
