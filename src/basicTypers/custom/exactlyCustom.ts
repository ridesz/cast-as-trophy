import { DisableNever } from "../never";
import { DisableUnion } from "../union";

// Just a remember: the T is not a Caster. It is a complex object.
type IsObject<T> = [T] extends [
    {
        /* Empty */
    },
]
    ? unknown
    : never;

// Disable union is necessary...
export type ExactlyCustom<T> = DisableNever<T> & DisableUnion<T> & IsObject<T>;
