import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyCustomArray } from "./exactlyCustomArray";

export type ExactlyCustomArrayOrUndefined<T> = ExactlyCustomArray<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
