import { DisableNever, DisableNeverArray } from "../never";
import { DisableUnion } from "../union";

// Just a remember: the T is not a Caster. It is a complex object.
type IsObject<T> = [T] extends [
    {
        /* Empty */
    },
]
    ? unknown
    : never;

// export type ExactlyCustom<T> = DisableNever<T> & DisableUnion<T> & IsObject<T>;

type ObjectArray<T> = [T] extends [Array<infer ELEMENT>] ? IsObject<ELEMENT> & DisableUnion<T> : never;

type DisableObjectTuple<T> = [T] extends [
    [
        {
            /* Emtpy */
        },
        ...unknown[]
    ],
]
    ? never
    : unknown;

export type ExactlyCustomArray<T> = DisableNever<T> & DisableNeverArray<T> & ObjectArray<T> & DisableObjectTuple<T>;
