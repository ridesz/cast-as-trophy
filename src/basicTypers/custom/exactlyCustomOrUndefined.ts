import { DisableNever } from "../never";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyCustom } from "./exactlyCustom";

export type ExactlyCustomOrUndefined<T> = DisableNever<T> & ExactlyCustom<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
