import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyCustomArray } from "./exactlyCustomArray";

export type ExactlyCustomArrayOrNull<T> = ExactlyCustomArray<RemoveNull<T>> & HasNullOrMore<T>;
