import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyCustom } from "./exactlyCustom";

export type ExactlyCustomOrNull<T> = DisableNever<T> & ExactlyCustom<RemoveNull<T>> & HasNullOrMore<T>;
