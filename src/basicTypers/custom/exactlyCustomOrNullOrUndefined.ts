import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyCustom } from "./exactlyCustom";

export type ExactlyCustomOrNullOrUndefined<T> = DisableNever<T> &
    ExactlyCustom<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
