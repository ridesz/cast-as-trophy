import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyCustomArray } from "./exactlyCustomArray";

export type ExactlyCustomArrayOrNullOrUndefined<T> = ExactlyCustomArray<RemoveNull<RemoveUndefined<T>>> &
    HasUndefinedOrMore<T> &
    HasNullOrMore<T>;
