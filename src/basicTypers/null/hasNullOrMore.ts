/**
 * Unknown when null is the part of the type (never when not)
 */
export type HasNullOrMore<T> = T extends null ? unknown : never;
