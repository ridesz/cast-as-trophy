export type WithoutNull<T> = T extends null ? never : T;
