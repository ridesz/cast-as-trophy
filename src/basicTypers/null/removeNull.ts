export type RemoveNull<T> = T extends null ? never : T;
