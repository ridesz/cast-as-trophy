/**
 * Unknown when undefined is the part of the type (never when not)
 */
export type HasUndefinedOrMore<T> = T extends undefined ? unknown : never;
