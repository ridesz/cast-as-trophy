import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyBooleanArray } from "./exactlyBooleanArray";

export type ExactlyBooleanArrayOrUndefined<T> = ExactlyBooleanArray<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
