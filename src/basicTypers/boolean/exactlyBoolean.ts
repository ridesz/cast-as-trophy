import { DisableNever } from "../never";

type NotOnlyTrue<T> = [T] extends [true] ? never : unknown;
type NotOnlyFalse<T> = [T] extends [false] ? never : unknown;
type NotBooleanLiteral<T> = NotOnlyTrue<T> & NotOnlyFalse<T>;

type IsBoolean<T> = [T] extends [boolean] ? unknown : never;

/**
 * Boolean type without union. (THIS CAN BE DANGEROUS WITH UNION TYPES LIKE UNDEFINED!)
 */
export type ExactlyBoolean<T> = DisableNever<T> & NotBooleanLiteral<T> & IsBoolean<T>;
