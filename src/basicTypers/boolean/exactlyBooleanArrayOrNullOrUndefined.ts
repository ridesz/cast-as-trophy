import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyBooleanArray } from "./exactlyBooleanArray";

export type ExactlyBooleanArrayOrNullOrUndefined<T> = ExactlyBooleanArray<RemoveNull<RemoveUndefined<T>>> &
    HasNullOrMore<T> &
    HasUndefinedOrMore<T>;
