import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyBoolean } from "./exactlyBoolean";

export type ExactlyBooleanOrNull<T> = DisableNever<T> & ExactlyBoolean<RemoveNull<T>> & HasNullOrMore<T>;
