import { DisableNever } from "../never";
import { HasNullOrMore, RemoveNull } from "../null";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyBoolean } from "./exactlyBoolean";

export type ExactlyBooleanOrNullOrUndefined<T> = DisableNever<T> &
    ExactlyBoolean<RemoveNull<RemoveUndefined<T>>> &
    HasNullOrMore<T> &
    HasUndefinedOrMore<T>;
