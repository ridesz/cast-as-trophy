import { DisableNever, DisableNeverArray } from "../never";
import { ExactlyBoolean } from "./exactlyBoolean";

type BooleanArray<T> = [T] extends [Array<infer ELEMENT>] ? ExactlyBoolean<ELEMENT> : never;

type DisableBooleanTuple<T> = [T] extends [[boolean, ...unknown[]]] ? never : unknown;

export type ExactlyBooleanArray<T> = DisableNever<T> & DisableNeverArray<T> & BooleanArray<T> & DisableBooleanTuple<T>;
