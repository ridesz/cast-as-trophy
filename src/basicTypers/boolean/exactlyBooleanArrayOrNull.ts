import { HasNullOrMore, RemoveNull } from "../null";
import { ExactlyBooleanArray } from "./exactlyBooleanArray";

export type ExactlyBooleanArrayOrNull<T> = ExactlyBooleanArray<RemoveNull<T>> & HasNullOrMore<T>;
