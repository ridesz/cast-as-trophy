import { DisableNever } from "../never";
import { HasUndefinedOrMore, RemoveUndefined } from "../undefined";
import { ExactlyBoolean } from "./exactlyBoolean";

export type ExactlyBooleanOrUndefined<T> = DisableNever<T> & ExactlyBoolean<RemoveUndefined<T>> & HasUndefinedOrMore<T>;
