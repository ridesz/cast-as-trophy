/**
 * Abstract class of the final object with casting capability.
 */
export abstract class Caster<TYPE> {
    public abstract readonly isType: (value: unknown) => value is TYPE;
    public abstract readonly copyTo: (value: unknown) => TYPE;
    public abstract readonly castTo: (value: unknown) => TYPE;
    public abstract readonly asyncCopyTo: (value: unknown) => Promise<TYPE>;
    public abstract readonly asyncCastTo: (value: unknown) => Promise<TYPE>;
}
