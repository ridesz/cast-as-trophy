import { stringArrayCaster } from "./stringArrayCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const stringArrayOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(stringArrayCaster);
