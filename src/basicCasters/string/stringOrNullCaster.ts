import { stringCaster } from "./stringCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const stringOrNullCaster = wrapAnObjectCasterToSupportNull(stringCaster);
