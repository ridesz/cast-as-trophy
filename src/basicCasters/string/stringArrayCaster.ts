import { stringCaster } from "./stringCaster";
import { wrapAnObjectCasterToSupportArray } from "../wrapAnObjectCasterToSupportArray";

export const stringArrayCaster = wrapAnObjectCasterToSupportArray(stringCaster);
