import { stringCaster } from "./stringCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const stringOrNullOrUndefinedCaster = wrapAnObjectCasterToSupportNull(
    wrapAnObjectCasterToSupportUndefined(stringCaster),
);
