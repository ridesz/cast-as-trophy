import { stringArrayCaster } from "./stringArrayCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const stringArrayOrNullOrUndefinedCaster = wrapAnObjectCasterToSupportNull(
    wrapAnObjectCasterToSupportUndefined(stringArrayCaster),
);
