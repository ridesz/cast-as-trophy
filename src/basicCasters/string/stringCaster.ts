import { Caster } from "../../caster";
import { throwException } from "../createException";

/**
 * Built-in caster for strings.
 */
export const stringCaster: Caster<string> = new (class extends Caster<string> {
    public readonly isType = (value: unknown): value is string => {
        return typeof value === "string";
    };
    public readonly castTo = (value: unknown): string => {
        if (this.isType(value)) {
            return value;
        }
        throw new Error("This value is not a string: " + String(value));
    };
    public readonly copyTo = (value: unknown): string => {
        if (this.isType(value)) {
            // https://stackoverflow.com/questions/31712808/how-to-force-javascript-to-deep-copy-a-string
            return (" " + value).slice(1);
        }
        throwException("string", value);
    };
    public readonly asyncCastTo = async (value: unknown): Promise<string> => {
        return this.castTo(value);
    };
    public readonly asyncCopyTo = async (value: unknown): Promise<string> => {
        return this.copyTo(value);
    };
})();
