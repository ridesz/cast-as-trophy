import { stringArrayCaster } from "./stringArrayCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const stringArrayOrNullCaster = wrapAnObjectCasterToSupportNull(stringArrayCaster);
