import { stringCaster } from "./stringCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const stringOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(stringCaster);
