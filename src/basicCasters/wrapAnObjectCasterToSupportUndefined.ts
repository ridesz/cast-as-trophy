import { Caster } from "../caster";

export function wrapAnObjectCasterToSupportUndefined<TYPE>(caster: Caster<TYPE>): Caster<TYPE | undefined> {
    const result = new (class extends Caster<TYPE | undefined> {
        public readonly isType = (value: unknown): value is TYPE | undefined => {
            if (typeof value === "undefined") {
                return true;
            }
            return caster.isType(value);
        };
        public readonly castTo = (value: unknown): TYPE | undefined => {
            if (typeof value === "undefined") {
                return undefined;
            }
            return caster.castTo(value);
        };
        public readonly copyTo = (value: unknown): TYPE | undefined => {
            if (typeof value === "undefined") {
                return undefined;
            }
            return caster.copyTo(value);
        };
        public readonly asyncCastTo = async (value: unknown): Promise<TYPE | undefined> => {
            if (typeof value === "undefined") {
                return undefined;
            }
            return caster.asyncCastTo(value);
        };
        public readonly asyncCopyTo = async (value: unknown): Promise<TYPE | undefined> => {
            if (typeof value === "undefined") {
                return undefined;
            }
            return caster.asyncCopyTo(value);
        };
    })();
    return result;
}
