import { Caster } from "../../caster";
import { throwException } from "../createException";

/**
 * Built-in caster for boolean.
 */
export const booleanCaster: Caster<boolean> = new (class extends Caster<boolean> {
    public readonly isType = (value: unknown): value is boolean => {
        return typeof value === "boolean";
    };
    public readonly castTo = (value: unknown): boolean => {
        return this.copyTo(value);
    };
    public readonly copyTo = (value: unknown): boolean => {
        if (this.isType(value)) {
            return value;
        }
        throwException("boolean", value);
    };
    public readonly asyncCastTo = async (value: unknown): Promise<boolean> => {
        return this.castTo(value);
    };
    public readonly asyncCopyTo = async (value: unknown): Promise<boolean> => {
        return this.copyTo(value);
    };
})();
