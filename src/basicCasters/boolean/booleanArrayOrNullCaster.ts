import { booleanArrayCaster } from "./booleanArrayCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const booleanArrayOrNullCaster = wrapAnObjectCasterToSupportNull(booleanArrayCaster);
