import { booleanArrayCaster } from "./booleanArrayCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const booleanArrayOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(booleanArrayCaster);
