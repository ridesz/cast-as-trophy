import { booleanCaster } from "./booleanCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const booleanOrNullCaster = wrapAnObjectCasterToSupportNull(booleanCaster);
