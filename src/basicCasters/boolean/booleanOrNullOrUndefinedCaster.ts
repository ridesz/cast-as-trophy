import { booleanCaster } from "./booleanCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const booleanOrNullOrUndefinedCaster = wrapAnObjectCasterToSupportNull(
    wrapAnObjectCasterToSupportUndefined(booleanCaster),
);
