import { booleanCaster } from "./booleanCaster";
import { wrapAnObjectCasterToSupportArray } from "../wrapAnObjectCasterToSupportArray";

export const booleanArrayCaster = wrapAnObjectCasterToSupportArray(booleanCaster);
