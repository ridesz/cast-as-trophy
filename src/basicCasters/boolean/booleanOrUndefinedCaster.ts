import { booleanCaster } from "./booleanCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const booleanOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(booleanCaster);
