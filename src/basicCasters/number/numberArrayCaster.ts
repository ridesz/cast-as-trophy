import { numberCaster } from "./numberCaster";
import { wrapAnObjectCasterToSupportArray } from "../wrapAnObjectCasterToSupportArray";

export const numberArrayCaster = wrapAnObjectCasterToSupportArray(numberCaster);
