import { numberCaster } from "./numberCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const numberOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(numberCaster);
