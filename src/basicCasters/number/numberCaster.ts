import { Caster } from "../../caster";
import { throwException } from "../createException";

/**
 * Built-in caster for numbers.
 */
export const numberCaster: Caster<number> = new (class extends Caster<number> {
    public readonly isType = (value: unknown): value is number => {
        return typeof value === "number";
    };
    public readonly castTo = (value: unknown): number => {
        return this.copyTo(value);
    };
    public readonly copyTo = (value: unknown): number => {
        if (this.isType(value)) {
            return value;
        }
        throwException("number", value);
    };
    public readonly asyncCastTo = async (value: unknown): Promise<number> => {
        return this.castTo(value);
    };
    public readonly asyncCopyTo = async (value: unknown): Promise<number> => {
        return this.copyTo(value);
    };
})();
