import { numberCaster } from "./numberCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const numberOrNullCaster = wrapAnObjectCasterToSupportNull(numberCaster);
