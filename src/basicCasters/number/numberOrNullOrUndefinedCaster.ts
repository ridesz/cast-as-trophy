import { numberCaster } from "./numberCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const numberOrNullOrUndefinedCaster = wrapAnObjectCasterToSupportNull(
    wrapAnObjectCasterToSupportUndefined(numberCaster),
);
