import { numberArrayCaster } from "./numberArrayCaster";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const numberArrayOrNullCaster = wrapAnObjectCasterToSupportNull(numberArrayCaster);
