import { numberArrayCaster } from "./numberArrayCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";
import { wrapAnObjectCasterToSupportNull } from "../wrapAnObjectCasterToSupportNull";

export const numberArrayOrNullOrUndefinedCaster = wrapAnObjectCasterToSupportNull(
    wrapAnObjectCasterToSupportUndefined(numberArrayCaster),
);
