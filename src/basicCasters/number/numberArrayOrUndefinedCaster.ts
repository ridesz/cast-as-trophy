import { numberArrayCaster } from "./numberArrayCaster";
import { wrapAnObjectCasterToSupportUndefined } from "../wrapAnObjectCasterToSupportUndefined";

export const numberArrayOrUndefinedCaster = wrapAnObjectCasterToSupportUndefined(numberArrayCaster);
