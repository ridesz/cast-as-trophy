import { Caster } from "../caster";

// Note: typeof null is object. JavaScript, you are awesome!
export function wrapAnObjectCasterToSupportNull<TYPE>(caster: Caster<TYPE>): Caster<TYPE | null> {
    const result = new (class extends Caster<TYPE | null> {
        public readonly isType = (value: unknown): value is TYPE | null => {
            if (value === null) {
                return true;
            }
            return caster.isType(value);
        };
        public readonly castTo = (value: unknown): TYPE | null => {
            if (value === null) {
                return null;
            }
            return caster.castTo(value);
        };
        public readonly copyTo = (value: unknown): TYPE | null => {
            if (value === null) {
                return null;
            }
            return caster.copyTo(value);
        };
        public readonly asyncCastTo = async (value: unknown): Promise<TYPE | null> => {
            if (value === null) {
                return null;
            }
            return caster.asyncCastTo(value);
        };
        public readonly asyncCopyTo = async (value: unknown): Promise<TYPE | null> => {
            if (value === null) {
                return null;
            }
            return caster.asyncCopyTo(value);
        };
    })();
    return result;
}
