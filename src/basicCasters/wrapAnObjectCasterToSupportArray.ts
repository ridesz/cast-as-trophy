import { Caster } from "../caster";

export function wrapAnObjectCasterToSupportArray<TYPE>(caster: Caster<TYPE>): Caster<TYPE[]> {
    // Preventing code duplication warning.
    const asyncCastToOrAsyncCopyTo = async (
        value: unknown,
        callable: (value: unknown) => Promise<TYPE>,
    ): Promise<TYPE[]> => {
        if (!Array.isArray(value)) {
            throw new Error("This value is not array: " + String(value));
        }
        const promises = value.map((element) => {
            if (element === undefined) {
                throw new Error("Undefined element in the array.");
            }
            return callable(element);
        });
        return await Promise.all(promises);
    };
    const result = new (class extends Caster<TYPE[]> {
        public readonly isType = (value: unknown): value is TYPE[] => {
            if (Array.isArray(value)) {
                return !value.some((element) => {
                    if (element === undefined) {
                        // This true means that the checking has been failed!
                        return true;
                    }
                    return !caster.isType(element);
                });
            }
            return false;
        };
        public readonly castTo = (value: unknown): TYPE[] => {
            if (Array.isArray(value)) {
                value.forEach((element) => {
                    if (element === undefined) {
                        throw new Error("Undefined element in the array.");
                    }
                    caster.castTo(element);
                });
                return value as TYPE[];
            }
            throw new Error("This value is not array: " + String(value));
        };
        public readonly copyTo = (value: unknown): TYPE[] => {
            const result: TYPE[] = [];
            if (Array.isArray(value)) {
                value.forEach((element) => {
                    if (element === undefined) {
                        throw new Error("Undefined element in the array.");
                    }
                    result.push(caster.copyTo(element));
                });
                return result;
            }
            throw new Error("This value is not array: " + String(value));
        };
        public readonly asyncCastTo = async (value: unknown): Promise<TYPE[]> => {
            await asyncCastToOrAsyncCopyTo(value, async (v: unknown) => {
                return caster.asyncCastTo(v);
            });
            return value as TYPE[];
        };
        public readonly asyncCopyTo = async (value: unknown): Promise<TYPE[]> => {
            return await asyncCastToOrAsyncCopyTo(value, async (v: unknown) => {
                return caster.asyncCopyTo(v);
            });
        };
    })();
    return result;
}
