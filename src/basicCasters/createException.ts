function generateStringValue(value: unknown): string {
    if (value === undefined) {
        return "undefined";
    } else if (value === null) {
        return "null";
    } else {
        return String(value);
    }
}

export function throwException(typeName: string, value: unknown): never {
    const stringValue = generateStringValue(value);
    throw new Error(`This value is not a ${typeName}: ${stringValue}`);
}
