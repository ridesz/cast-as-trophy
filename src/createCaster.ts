import {
    KeyOfAddString,
    KeyOfAddNumber,
    KeyOfAddStringOrUndefined,
    KeyOfAddNumberOrUndefined,
    KeyOfAddBoolean,
    KeyOfAddBooleanOrUndefined,
    KeyOfAddStringArray,
    KeyOfAddNumberArray,
    KeyOfAddBooleanArray,
    KeyOfAddCustomArray,
    KeyOfAddCustom,
    KeyOfAddCustomOrUndefined,
    KeyOfAddCustomArrayOrUndefined,
    KeyOfAddStringArrayOrUndefined,
    KeyOfAddNumberArrayOrUndefined,
    KeyOfAddBooleanArrayOrUndefined,
    KeyOfAddStringOrNull,
    KeyOfAddStringOrNullOrUndefined,
    KeyOfAddNumberOrNull,
    KeyOfAddNumberOrNullOrUndefined,
    KeyOfAddBooleanOrNull,
    KeyOfAddBooleanOrNullOrUndefined,
    KeyOfAddStringArrayOrNull,
    KeyOfAddStringArrayOrNullOrUndefined,
    KeyOfAddNumberArrayOrNull,
    KeyOfAddNumberArrayOrNullOrUndefined,
    KeyOfAddBooleanArrayOrNull,
    KeyOfAddBooleanArrayOrNullOrUndefined,
    KeyOfAddCustomOrNull,
    KeyOfAddCustomOrNullOrUndefined,
    KeyOfAddCustomArrayOrNull,
    KeyOfAddCustomArrayOrNullOrUndefined,
} from "./keyTypers";
import {
    stringCaster,
    numberCaster,
    booleanCaster,
    wrapAnObjectCasterToSupportUndefined,
    stringOrUndefinedCaster,
    numberOrUndefinedCaster,
    booleanOrUndefinedCaster,
    wrapAnObjectCasterToSupportArray,
    stringArrayCaster,
    numberArrayCaster,
    booleanArrayCaster,
    stringArrayOrUndefinedCaster,
    numberArrayOrUndefinedCaster,
    booleanArrayOrUndefinedCaster,
    stringOrNullCaster,
    stringOrNullOrUndefinedCaster,
    numberOrNullCaster,
    numberOrNullOrUndefinedCaster,
    booleanOrNullCaster,
    booleanOrNullOrUndefinedCaster,
    stringArrayOrNullCaster,
    stringArrayOrNullOrUndefinedCaster,
    numberArrayOrNullCaster,
    numberArrayOrNullOrUndefinedCaster,
    booleanArrayOrNullCaster,
    booleanArrayOrNullOrUndefinedCaster,
    wrapAnObjectCasterToSupportNull,
} from "./basicCasters";
import { WithoutNull, WithoutUndefined } from "./basicTypers";
import { Caster } from "./caster";

type CasterForArrayOrUndefined<T> = T extends Array<infer ELEMENT> ? Caster<WithoutUndefined<ELEMENT>> : never;

type CasterForArrayOrNullOrUndefined<T> = T extends Array<infer ELEMENT>
    ? Caster<WithoutNull<WithoutUndefined<ELEMENT>>>
    : never;

type CasterForArrayOrNull<T> = T extends Array<infer ELEMENT> ? Caster<WithoutNull<ELEMENT>> : never;

/**
 * Interface for a finished caster builder.
 */
interface ICasterBuilderReadyToBuild<TYPE> {
    build(): Caster<TYPE>;
}

/**
 * Partial casters type (casters for the already handled fields)
 *
 * Keys are limited, values for keys are not so limited (type for remaining key is enabled, but only hypothetically)
 */
type PartialCastersType<TYPE, REMAINING> = {
    [index in keyof Omit<TYPE, keyof REMAINING>]: Caster<TYPE[index]>;
};

/**
 * Type depending on the remaining part:
 * - We can't build the final object when the remaining part is not empty
 * - We can build the final object when the remaining part is empty
 */
type CasterBuilderReturnType<
    TYPE extends REMAINING,
    REMAINING extends {
        /* EMPTY*/
    },
> = keyof REMAINING extends never ? ICasterBuilderReadyToBuild<TYPE> : ICasterBuilderInProgress<TYPE, REMAINING>;

/**
 * Interface for a caster builder in progress.
 */
interface ICasterBuilderInProgress<
    TYPE extends REMAINING,
    REMAINING extends {
        /* EMPTY */
    },
> {
    /**
     * Add string field
     *
     * @param key Key of a string typed property
     */
    addString<KEY extends keyof REMAINING>(
        key: KeyOfAddString<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string or undefined field
     *
     * @param key Key
     */
    addStringOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string or null field
     *
     * @param key Key
     */
    addStringOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string or null or undefined field
     *
     * @param key Key
     */
    addStringOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number field
     *
     * @param key Key
     */
    addNumber<KEY extends keyof REMAINING>(
        key: KeyOfAddNumber<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number or undefined field
     *
     * @param key Key
     */
    addNumberOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number or null field
     *
     * @param key Key
     */
    addNumberOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number or null or undefined field
     *
     * @param key Key
     */
    addNumberOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean field
     *
     * @param key Key
     */
    addBoolean<KEY extends keyof REMAINING>(
        key: KeyOfAddBoolean<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean or undefined field
     *
     * @param key Key
     */
    addBooleanOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean or null field
     *
     * @param key Key
     */
    addBooleanOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean or null or undefined field
     *
     * @param key Key
     */
    addBooleanOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add custom field
     *
     * @param key Key
     * @param caster Caster
     */
    addCustom<KEY extends keyof REMAINING>(
        key: KeyOfAddCustom<REMAINING, KEY>,
        caster: Caster<REMAINING[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add custom or undefined field
     *
     * @param key Key
     * @param caster Caster
     */
    addCustomOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrUndefined<REMAINING, KEY>,
        caster: Caster<WithoutUndefined<REMAINING[KEY]>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add custom or null field
     *
     * @param key Key
     * @param caster Caster
     */
    addCustomOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrNull<REMAINING, KEY>,
        caster: Caster<WithoutNull<REMAINING[KEY]>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add custom or null or undefined field
     *
     * @param key Key
     * @param caster Caster
     */
    addCustomOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrNullOrUndefined<REMAINING, KEY>,
        caster: Caster<WithoutNull<WithoutUndefined<REMAINING[KEY]>>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string array field
     *
     * @param key Key
     */
    addStringArray<KEY extends keyof REMAINING>(
        key: KeyOfAddStringArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string array or undefined field
     *
     * @param key Key
     */
    addStringArrayOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string array or null field
     *
     * @param key Key
     */
    addStringArrayOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add string array or null or undefined field
     *
     * @param key Key
     */
    addStringArrayOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number array field
     *
     * @param key Key
     */
    addNumberArray<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number array or undefined field
     *
     * @param key Key
     */
    addNumberArrayOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number array or null field
     *
     * @param key Key
     */
    addNumberArrayOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add number array or null or undefined field
     *
     * @param key Key
     */
    addNumberArrayOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean array field
     *
     * @param key Key
     */
    addBooleanArray<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean array or undefined field
     *
     * @param key Key
     */
    addBooleanArrayOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean array or null field
     *
     * @param key Key
     */
    addBooleanArrayOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add boolean array or null or undefined field
     *
     * @param key Key
     */
    addBooleanArrayOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    /**
     * Add custom field
     *
     * @param key Key
     * @param caster Caster
     */
    addCustomArray<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArray<REMAINING, KEY>,
        caster: TYPE[KEY] extends Array<infer ELEMENT> ? Caster<ELEMENT> : never,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    addCustomArrayOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrUndefined<REMAINING, KEY>,
        caster: CasterForArrayOrUndefined<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    addCustomArrayOrNull<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrNull<REMAINING, KEY>,
        caster: CasterForArrayOrNull<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;

    addCustomArrayOrNullOrUndefined<KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrNullOrUndefined<REMAINING, KEY>,
        caster: CasterForArrayOrNullOrUndefined<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>>;
}

/**
 * Build a given caster from a partial caster object;
 *
 * @param partialCasters Partial casters
 */
const buildCaster = <
    TYPE extends REMAINING,
    REMAINING extends {
        /* EMPTY*/
    },
>(
    partialCasters: PartialCastersType<TYPE, REMAINING>,
): Caster<TYPE> => {
    const doSync = (
        value: unknown,
        callCaster: (
            caster: PartialCastersType<TYPE, REMAINING>[Exclude<keyof TYPE, keyof REMAINING>],
            value: unknown,
            key: Exclude<keyof TYPE, keyof REMAINING>,
        ) => void,
    ): void => {
        if (typeof value !== "object" || value === null) {
            throw new Error("The value is not an object.");
        }
        const keys = Object.keys(partialCasters) as Array<keyof PartialCastersType<TYPE, REMAINING>>;
        keys.forEach((key) => {
            const caster = partialCasters[key];
            const typedValue = value as { [index in keyof TYPE]: TYPE[index] };
            try {
                callCaster(caster, typedValue[key], key);
            } catch (error) {
                throw new Error(`${String(key)} -> ${(error as Error).message}`);
            }
        });
    };
    const doAsync = (
        value: unknown,
        callCaster: (
            caster: PartialCastersType<TYPE, REMAINING>[Exclude<keyof TYPE, keyof REMAINING>],
            value: unknown,
            key: Exclude<keyof TYPE, keyof REMAINING>,
        ) => Promise<void>,
    ): Array<Promise<void>> => {
        if (typeof value !== "object" || value === null) {
            throw new Error("The value is not an object.");
        }
        const keys = Object.keys(partialCasters) as Array<keyof PartialCastersType<TYPE, REMAINING>>;
        const promises = keys.map(async (key) => {
            const caster = partialCasters[key];
            const typedValue = value as { [index in keyof TYPE]: TYPE[index] };
            try {
                await callCaster(caster, typedValue[key], key);
            } catch (error) {
                throw new Error(`${String(key)} -> ${(error as Error).message}`);
            }
        });
        return promises;
    };
    const CasterClass = class extends Caster<TYPE> {
        public readonly isType = (value: unknown): value is TYPE => {
            if (typeof value !== "object" || value === null) {
                return false;
            }
            let result = true;
            const keys = Object.keys(partialCasters) as Array<keyof PartialCastersType<TYPE, REMAINING>>;
            keys.forEach((key) => {
                const caster = partialCasters[key];
                const typedValue = value as { [index in keyof TYPE]: TYPE[index] };
                if (!caster.isType(typedValue[key])) {
                    result = false;
                }
            });
            return result;
        };
        public readonly castTo = (value: unknown): TYPE => {
            doSync(value, (c, v) => {
                c.castTo(v);
            });
            return value as TYPE;
        };
        public readonly copyTo = (value: unknown): TYPE => {
            const result: { [index in keyof TYPE]?: TYPE[index] } = {};
            doSync(value, (c, v, k) => {
                result[k] = c.copyTo(v);
            });
            return result as TYPE;
        };
        public readonly asyncCastTo = async (value: unknown): Promise<TYPE> => {
            const promises = await doAsync(value, async (c, v) => {
                await c.asyncCastTo(v);
            });
            await Promise.all(promises);
            return value as TYPE;
        };
        public readonly asyncCopyTo = async (value: unknown): Promise<TYPE> => {
            const result: { [index in keyof TYPE]?: TYPE[index] } = {};
            const promises = doAsync(value, async (c, v, k) => {
                result[k] = await c.asyncCopyTo(v);
            });
            await Promise.all(promises);
            return result as TYPE;
        };
    };
    return new CasterClass();
};

class CasterBuilder<
    TYPE extends REMAINING,
    REMAINING extends {
        /* EMPTY*/
    },
> implements ICasterBuilderReadyToBuild<TYPE>, ICasterBuilderInProgress<TYPE, REMAINING>
{
    /**
     * The current casters we have to build the final solution.
     */
    private partialCasters: PartialCastersType<TYPE, REMAINING>;

    /**
     * Just a good old friendly private constructor. Nothing to see here.
     * Please use the `createInstance` method to create new instances! Thanks!
     *
     * @param partialCasters current casters
     */
    private constructor(partialCasters: PartialCastersType<TYPE, REMAINING>) {
        this.partialCasters = partialCasters;
    }

    /**
     * Static function to create a new instance (instead of constructor. Constructors are evil.)
     */
    public static createInstance = <
        INSTANCE_TYPE extends {
            /* EMPTY*/
        },
    >(): CasterBuilder<INSTANCE_TYPE, INSTANCE_TYPE> => {
        return new CasterBuilder<INSTANCE_TYPE, INSTANCE_TYPE>({});
    };

    private static updatePartialCasters = <
        TYPE extends REMAINING,
        REMAINING extends {
            /* EMPTY*/
        },
        KEY extends keyof TYPE,
    >(
        partialCasters: PartialCastersType<TYPE, REMAINING>,
        key: keyof TYPE,
        caster: Caster<unknown>, // TODO: This is not so great... Maybe we could do this better.
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        const newPartialCasters = {
            ...partialCasters,
            [key]: caster,
        } as unknown as PartialCastersType<TYPE, Omit<REMAINING, KEY>>;
        return new CasterBuilder<TYPE, Omit<REMAINING, KEY>>(newPartialCasters) as unknown as CasterBuilderReturnType<
            TYPE,
            Omit<REMAINING, KEY>
        >;
    };

    /**
     * Add a string checker to a key.
     *
     * @param key Key of a string typed property
     */
    public readonly addString = <KEY extends keyof REMAINING>(
        key: KeyOfAddString<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringCaster);
    };

    /**
     * Add a string or undefined checker to a key.
     */
    public readonly addStringOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringOrUndefinedCaster);
    };

    /**
     * Add a string or null checker to a key.
     */
    public readonly addStringOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringOrNullCaster);
    };

    /**
     * Add a string or null or undefined checker to a key.
     */
    public readonly addStringOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringOrNullOrUndefinedCaster);
    };

    /**
     * Add a number checker to a key.
     */
    public readonly addNumber = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumber<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberCaster);
    };

    /**
     * Add a number or undefined checker to a key.
     */
    public readonly addNumberOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberOrUndefinedCaster);
    };

    /**
     * Add a number or null checker to a key.
     */
    public readonly addNumberOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberOrNullCaster);
    };

    /**
     * Add a number or null or undefined checker to a key.
     */
    public readonly addNumberOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberOrNullOrUndefinedCaster);
    };

    /**
     * Add a boolean checker to a key.
     */
    public readonly addBoolean = <KEY extends keyof REMAINING>(
        key: KeyOfAddBoolean<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanCaster);
    };

    /**
     * Add a boolean or undefined checker to a key.
     */
    public readonly addBooleanOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanOrUndefinedCaster);
    };

    /**
     * Add a boolean or null checker to a key.
     */
    public readonly addBooleanOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanOrNullCaster);
    };

    /**
     * Add a boolean or null or undefined checker to a key.
     */
    public readonly addBooleanOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanOrNullOrUndefinedCaster);
    };

    /**
     * You can't have caster for a non-compatible type. Or can you have?
     */

    public readonly addCustom = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustom<REMAINING, KEY>,
        caster: Caster<WithoutUndefined<TYPE[KEY]>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, caster);
    };

    public readonly addCustomOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrUndefined<REMAINING, KEY>,
        caster: Caster<WithoutUndefined<TYPE[KEY]>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(
            this.partialCasters,
            key,
            wrapAnObjectCasterToSupportUndefined(caster),
        );
    };

    public readonly addCustomOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrNull<REMAINING, KEY>,
        caster: Caster<WithoutNull<TYPE[KEY]>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, wrapAnObjectCasterToSupportNull(caster));
    };

    public readonly addCustomOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomOrNullOrUndefined<REMAINING, KEY>,
        caster: Caster<WithoutNull<WithoutUndefined<TYPE[KEY]>>>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(
            this.partialCasters,
            key,
            wrapAnObjectCasterToSupportNull(wrapAnObjectCasterToSupportUndefined(caster)),
        );
    };

    public readonly addStringArray = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringArrayCaster);
    };

    public readonly addStringArrayOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringArrayOrUndefinedCaster);
    };

    public readonly addStringArrayOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringArrayOrNullCaster);
    };

    public readonly addStringArrayOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddStringArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, stringArrayOrNullOrUndefinedCaster);
    };

    public readonly addNumberArray = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberArrayCaster);
    };

    public readonly addNumberArrayOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberArrayOrUndefinedCaster);
    };

    public readonly addNumberArrayOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberArrayOrNullCaster);
    };

    public readonly addNumberArrayOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddNumberArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, numberArrayOrNullOrUndefinedCaster);
    };

    public readonly addBooleanArray = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArray<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanArrayCaster);
    };

    public readonly addBooleanArrayOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanArrayOrUndefinedCaster);
    };

    public readonly addBooleanArrayOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrNull<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanArrayOrNullCaster);
    };

    public readonly addBooleanArrayOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddBooleanArrayOrNullOrUndefined<REMAINING, KEY>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, booleanArrayOrNullOrUndefinedCaster);
    };

    public readonly addCustomArray = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArray<REMAINING, KEY>,
        caster: TYPE[KEY] extends Array<infer ELEMENT> ? Caster<WithoutUndefined<ELEMENT>> : never,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(this.partialCasters, key, wrapAnObjectCasterToSupportArray(caster));
    };

    public readonly addCustomArrayOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrUndefined<REMAINING, KEY>,
        caster: CasterForArrayOrUndefined<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(
            this.partialCasters,
            key,
            wrapAnObjectCasterToSupportUndefined(wrapAnObjectCasterToSupportArray(caster)),
        );
    };

    public readonly addCustomArrayOrNull = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrNull<REMAINING, KEY>,
        caster: CasterForArrayOrNull<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(
            this.partialCasters,
            key,
            wrapAnObjectCasterToSupportNull(wrapAnObjectCasterToSupportArray(caster)),
        );
    };

    public readonly addCustomArrayOrNullOrUndefined = <KEY extends keyof REMAINING>(
        key: KeyOfAddCustomArrayOrNullOrUndefined<REMAINING, KEY>,
        caster: CasterForArrayOrNullOrUndefined<TYPE[KEY]>,
    ): CasterBuilderReturnType<TYPE, Omit<REMAINING, KEY>> => {
        return CasterBuilder.updatePartialCasters(
            this.partialCasters,
            key,
            wrapAnObjectCasterToSupportNull(
                wrapAnObjectCasterToSupportUndefined(wrapAnObjectCasterToSupportArray(caster)),
            ),
        );
    };

    /**
     * Build the final type checker.
     */
    public readonly build = (): Caster<TYPE> => {
        return buildCaster<TYPE, REMAINING>(this.partialCasters);
    };
}

/**
 * Create a new caster builder.
 *
 * The default value is `never` just to be sure that there will be a value for the `TYPE`.
 */
export function createCaster<
    TYPE extends {
        /* EMPTY*/
    } = never,
>(): CasterBuilderReturnType<TYPE, TYPE> {
    const newCasterInstance: CasterBuilder<TYPE, TYPE> = CasterBuilder.createInstance<TYPE>();
    return newCasterInstance as unknown as CasterBuilderReturnType<TYPE, TYPE>;
}
