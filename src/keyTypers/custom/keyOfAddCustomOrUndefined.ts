import { ExactlyCustomOrUndefined } from "../../basicTypers";

export type KeyOfAddCustomOrUndefined<TYPE, KEY extends keyof TYPE> = KEY & ExactlyCustomOrUndefined<TYPE[KEY]>;
