import { ExactlyCustomArrayOrUndefined } from "../../basicTypers";

export type KeyOfAddCustomArrayOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyCustomArrayOrUndefined<TYPE[KEY]>;
