import { ExactlyCustomOrNull } from "../../basicTypers";

export type KeyOfAddCustomOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyCustomOrNull<TYPE[KEY]>;
