import { ExactlyCustomArrayOrNull } from "../../basicTypers";

export type KeyOfAddCustomArrayOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyCustomArrayOrNull<TYPE[KEY]>;
