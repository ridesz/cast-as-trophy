import { ExactlyCustomOrNullOrUndefined } from "../../basicTypers";

export type KeyOfAddCustomOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyCustomOrNullOrUndefined<TYPE[KEY]>;
