import { ExactlyCustom } from "../../basicTypers";

/**
 * Custom
 */
export type KeyOfAddCustom<TYPE, KEY extends keyof TYPE> = KEY & ExactlyCustom<TYPE[KEY]>;
