import { ExactlyCustomArrayOrNullOrUndefined } from "../../basicTypers";

export type KeyOfAddCustomArrayOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyCustomArrayOrNullOrUndefined<TYPE[KEY]>;
