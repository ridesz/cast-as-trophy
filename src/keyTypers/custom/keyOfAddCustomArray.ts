import { ExactlyCustomArray } from "../../basicTypers";

/**
 * Custom array key
 */
export type KeyOfAddCustomArray<TYPE, KEY extends keyof TYPE> = KEY & ExactlyCustomArray<TYPE[KEY]>;
