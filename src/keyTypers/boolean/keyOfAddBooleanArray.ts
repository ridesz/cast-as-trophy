import { ExactlyBooleanArray } from "../../basicTypers";

/**
 * Boolean array key
 */
export type KeyOfAddBooleanArray<TYPE, KEY extends keyof TYPE> = KEY & ExactlyBooleanArray<TYPE[KEY]>;
