import { ExactlyBooleanArrayOrNullOrUndefined } from "../../basicTypers";

/**
 * Boolean array or null or undefined key type
 */
export type KeyOfAddBooleanArrayOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyBooleanArrayOrNullOrUndefined<TYPE[KEY]>;
