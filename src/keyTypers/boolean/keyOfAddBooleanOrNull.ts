import { ExactlyBooleanOrNull } from "../../basicTypers";

/**
 * Type of a key of a property with boolean or null type.
 */
export type KeyOfAddBooleanOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyBooleanOrNull<TYPE[KEY]>;
