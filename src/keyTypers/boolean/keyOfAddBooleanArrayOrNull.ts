import { ExactlyBooleanArrayOrNull } from "../../basicTypers";

/**
 * Boolean array or null key type
 */
export type KeyOfAddBooleanArrayOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyBooleanArrayOrNull<TYPE[KEY]>;
