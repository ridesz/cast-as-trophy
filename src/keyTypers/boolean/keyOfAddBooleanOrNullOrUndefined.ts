import { ExactlyBooleanOrNullOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with boolean or null or undefined type.
 */
export type KeyOfAddBooleanOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyBooleanOrNullOrUndefined<TYPE[KEY]>;
