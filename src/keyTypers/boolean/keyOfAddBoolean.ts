import { ExactlyBoolean } from "../../basicTypers";

/**
 * Type of a key of a property with boolean type (without union type).
 */
export type KeyOfAddBoolean<TYPE, KEY extends keyof TYPE> = KEY & ExactlyBoolean<TYPE[KEY]>;
