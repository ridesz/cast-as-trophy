import { ExactlyBooleanOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with boolean or undefined type.
 */
export type KeyOfAddBooleanOrUndefined<TYPE, KEY extends keyof TYPE> = KEY & ExactlyBooleanOrUndefined<TYPE[KEY]>;
