import { ExactlyBooleanArrayOrUndefined } from "../../basicTypers";

/**
 * Boolean array or undefined key type
 */
export type KeyOfAddBooleanArrayOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyBooleanArrayOrUndefined<TYPE[KEY]>;
