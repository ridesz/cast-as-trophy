import { ExactlyStringArray } from "../../basicTypers";

/**
 * String array key
 */
export type KeyOfAddStringArray<TYPE, KEY extends keyof TYPE> = KEY & ExactlyStringArray<TYPE[KEY]>;
