import { ExactlyStringOrNullOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with string or null or undefined type.
 */
export type KeyOfAddStringOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyStringOrNullOrUndefined<TYPE[KEY]>;
