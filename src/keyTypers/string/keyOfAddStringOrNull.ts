import { ExactlyStringOrNull } from "../../basicTypers";

/**
 * Type of a key of a property with string or null type.
 */
export type KeyOfAddStringOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyStringOrNull<TYPE[KEY]>;
