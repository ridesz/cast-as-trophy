import { ExactlyStringArrayOrNull } from "../../basicTypers";

/**
 * String array or null key type
 */
export type KeyOfAddStringArrayOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyStringArrayOrNull<TYPE[KEY]>;
