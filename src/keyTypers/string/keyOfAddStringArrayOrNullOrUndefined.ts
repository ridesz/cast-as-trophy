import { ExactlyStringArrayOrNullOrUndefined } from "../../basicTypers";

/**
 * String array or null or undefined key type
 */
export type KeyOfAddStringArrayOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyStringArrayOrNullOrUndefined<TYPE[KEY]>;
