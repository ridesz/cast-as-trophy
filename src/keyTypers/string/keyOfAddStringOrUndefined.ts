import { ExactlyStringOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with string or undefined type.
 */
export type KeyOfAddStringOrUndefined<TYPE, KEY extends keyof TYPE> = KEY & ExactlyStringOrUndefined<TYPE[KEY]>;
