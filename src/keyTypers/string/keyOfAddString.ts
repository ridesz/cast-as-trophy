import { ExactlyString } from "../../basicTypers";

/**
 * Type of a key of a property with string type (without union type).
 */
export type KeyOfAddString<TYPE, KEY extends keyof TYPE> = KEY & ExactlyString<TYPE[KEY]>;
