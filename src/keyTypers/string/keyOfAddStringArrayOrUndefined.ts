import { ExactlyStringArrayOrUndefined } from "../../basicTypers";

/**
 * String array or undefined key type
 */
export type KeyOfAddStringArrayOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyStringArrayOrUndefined<TYPE[KEY]>;
