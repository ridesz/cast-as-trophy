import { ExactlyNumberOrNullOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with number or null orundefined type.
 */
export type KeyOfAddNumberOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyNumberOrNullOrUndefined<TYPE[KEY]>;
