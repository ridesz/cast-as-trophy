import { ExactlyNumberArrayOrNullOrUndefined } from "../../basicTypers";

/**
 * Number array or null or undefined key type
 */
export type KeyOfAddNumberArrayOrNullOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyNumberArrayOrNullOrUndefined<TYPE[KEY]>;
