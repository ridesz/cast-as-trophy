import { ExactlyNumberArray } from "../../basicTypers";

/**
 * Number array key
 */
export type KeyOfAddNumberArray<TYPE, KEY extends keyof TYPE> = KEY & ExactlyNumberArray<TYPE[KEY]>;
