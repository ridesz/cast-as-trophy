import { ExactlyNumberArrayOrUndefined } from "../../basicTypers";

/**
 * Number array or undefined key type
 */
export type KeyOfAddNumberArrayOrUndefined<TYPE, KEY extends keyof TYPE> = KEY &
    ExactlyNumberArrayOrUndefined<TYPE[KEY]>;
