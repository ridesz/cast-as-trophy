import { ExactlyNumberOrNull } from "../../basicTypers";

/**
 * Type of a key of a property with number or null type.
 */
export type KeyOfAddNumberOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyNumberOrNull<TYPE[KEY]>;
