import { ExactlyNumberArrayOrNull } from "../../basicTypers";

/**
 * Number array or null key type
 */
export type KeyOfAddNumberArrayOrNull<TYPE, KEY extends keyof TYPE> = KEY & ExactlyNumberArrayOrNull<TYPE[KEY]>;
