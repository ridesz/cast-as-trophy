import { ExactlyNumberOrUndefined } from "../../basicTypers";

/**
 * Type of a key of a property with number or undefined type.
 */
export type KeyOfAddNumberOrUndefined<TYPE, KEY extends keyof TYPE> = KEY & ExactlyNumberOrUndefined<TYPE[KEY]>;
