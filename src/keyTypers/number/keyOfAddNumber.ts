import { ExactlyNumber } from "../../basicTypers";

/**
 * Type of a key of a property with number type (without union type).
 */
export type KeyOfAddNumber<TYPE, KEY extends keyof TYPE> = KEY & ExactlyNumber<TYPE[KEY]>;
